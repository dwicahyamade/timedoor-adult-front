<a href="#" target="_blank" class="btn-whatsapp">
  <i class="fab fa-whatsapp"></i>
  <div class="btn-whatsapp__text">
    <p class="btn-whatsapp__title">Need Help?</p>
    <span class="btn-whatsapp__desc">Chat with Mentor</span>
  </div>
</a>