<header class="nav-custom">
  <nav class="navbar navbar-expand-lg">
    <div class="container container-custom">
      <a class="navbar-brand navbar-brand-custom" href="dashboard.php"><img src="img/timedoor-logo-black.png" alt="Timedoor Logo"
          width="141" height="33"></a>
      <ul class="navbar-nav flex-row">
        <li class="nav-item pr-3">
          <a class="nav-link nav-custom__link <?php if($_SERVER['SCRIPT_NAME']=="/dashboard.php") {?> active <?php } ?> "
            href="dashboard.php">Dashboard</a>
        </li>
        <li class="nav-item pr-3">
          <a class="nav-link nav-custom__link <?php if($_SERVER['SCRIPT_NAME']=="/submission.php") {?> active <?php } ?> "
            href="submission.php">Submission List</a>
        </li>
      </ul>
      <div class="ml-auto">
        <div class="btn-group">
          <button class="bg-transparent border-0 mr-3 nav-custom__notification" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false"><i class="far fa-bell fa-lg"></i>
          </button>
          <div class="dropdown-menu dropdown-menu-right dropdown-custom-nav__notification">
            <div class="dropdown-custom-nav__head">Notification <span class="badge badge-pill badge-red">6
                New</span>
            </div>
            <div class="dropdown-divider m-0"></div>
            <a class="dropdown-item dropdown-custom-nav__item dropdown-custom-nav__arrow py-2" href="#">
              <img src="img/icon/icon-code.svg" alt="Notification" class="mr-3">
              <p class="mb-0">Your submission has been comented
                <span class="d-block dropdown-custom-nav__time">just now</span>
              </p>
            </a>
            <div class="dropdown-divider m-0"></div>
            <a class="dropdown-item dropdown-custom-nav__item dropdown-custom-nav__arrow py-2" href="#">
              <img src="img/icon/icon-whatsapp.svg" alt="Notification" class="mr-3">
              <p class="mb-0">You got a message from tutor
                John Doe
                <span class="d-block dropdown-custom-nav__time">5 minutes ago</span>
              </p>
            </a>
            <div class="dropdown-divider m-0"></div>
            <a class="dropdown-item dropdown-custom-nav__item dropdown-custom-nav__arrow py-2" href="#">
              <img src="img/icon/icon-lock-nav.svg" alt="Notification" style="margin-right: 22px;">
              <p class="mb-0">Edit your privacy. Go to account
                setting
                <span class="d-block dropdown-custom-nav__time">10 minutes ago</span>
              </p>
            </a>
            <div class="dropdown-divider m-0"></div>
            <a class="dropdown-custom-nav__footer justify-content-center" href="#">See More
            </a>
          </div>
        </div>

        <div class="btn-group">
          <button type="button" class="bg-transparent border-0" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <img src="img/user-default-sm.png" alt="" width="42" height="42" class="rounded-circle nav-custom__profile">
          </button>
          <div class="dropdown-menu dropdown-menu-right dropdown-custom-nav">
            <a class="dropdown-item dropdown-custom-nav__item" href="account-profile.php">Profile</a>
            <a class="dropdown-item dropdown-custom-nav__item" href="account-info.php">Account Setting</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item dropdown-custom-nav__item--green" href="index.php">Log Out</a>
          </div>
        </div>
      </div>
    </div>
  </nav>
</header>