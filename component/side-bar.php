<div class="side-bar">
  <div class="side-bar__head">
    <h2 class="side-bar__title">Material List</h2>
    <i class="fas fa-times fa-2x side-bar__btn-close"></i>
  </div>
  <!-- Free Course -->
  <div class="accordion" id="Introduction">
    <div class="border-0">
      <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
        <a href="#" class="btn btn-accordion btn-block" type="button" data-toggle="collapse" data-target="#introduction"
          aria-expanded="true" aria-controls="introduction">
          <h3 class="section__title side-bar__subtitle">Free Trial Course
            <span class="side-bar__subtitle--small">8/8 Complete</span>
          </h3>
          <span class="btn-accordion__icon"></span>
        </a>
      </div>
      <div id="introduction" class="collapse" aria-labelledby="headingOne" data-parent="#Introduction">
        <div class="accordion" id="IntroductionTopic">
          <div class="accordion-border">
            <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
              data-toggle="collapse" data-target="#topicOne" aria-expanded="true" aria-controls="topicOne" id="topic">
              <h4 class="section__title--top topic__title">Topic 1 - Digital Introduction</h4>

            </button>
            <div id="topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
              data-parent="#IntroductionTopic">
              <div class="topic-inner__container">
                <div class="topic-inner__detail my-2">
                  <div class="form-check d-inline-block">
                    <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                  </div>
                  <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                  <p class="topic__progress-text">Hello World</p>
                </div>
                <div class="topic-inner__detail my-2">
                  <div class="form-check d-inline-block">
                    <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                  </div>
                  <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                  <p class="topic__progress-text">Italic, bold, underline</p>
                </div>
                <div class="topic-inner__detail my-2">
                  <div class="form-check d-inline-block">
                    <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                  </div>
                  <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                  <p class="topic__progress-text">Image</p>
                </div>
                <div class="topic-inner__detail my-2">
                  <div class="form-check d-inline-block">
                    <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                  </div>
                  <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i>
                    Practice</a>
                  <p class="topic__progress-text">Outline of material</p>
                </div>
                <div class="topic-inner__detail my-2">
                  <div class="form-check d-inline-block">
                    <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                  </div>
                  <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i>
                    Practice</a>
                  <p class="topic__progress-text">Explain mentor system</p>
                </div>
              </div>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#topicTwo" aria-expanded="true" aria-controls="topicTwo">
            <h4 class="section__title--top topic__title">Topic 2 - Coding Practice</h4>
          </button>
          <div id="topicTwo" class="collapse topic-inner" aria-labelledby="headingOne" data-parent="#IntroductionTopic">
            <div class="topic-inner__container">
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                <p class="topic__progress-text">Hello World</p>
              </div>
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                <p class="topic__progress-text">Italic, bold, underline</p>
              </div>
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                <p class="topic__progress-text">Image</p>
              </div>
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i> Practice</a>
                <p class="topic__progress-text">Outline of material</p>
              </div>
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i> Practice</a>
                <p class="topic__progress-text">Explain mentor system</p>
              </div>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#topicThree" aria-expanded="true" aria-controls="topicThree">
            <h4 class="section__title--top topic__title">Topic 3 - Introduction of HTML</h4>
          </button>
          <div id="topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#IntroductionTopic">
            <div class="topic-inner__container">
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                <p class="topic__progress-text">Hello World</p>
              </div>
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                <p class="topic__progress-text">Italic, bold, underline</p>
              </div>
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                <p class="topic__progress-text">Image</p>
              </div>
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i> Practice</a>
                <p class="topic__progress-text">Outline of material</p>
              </div>
              <div class="topic-inner__detail my-2">
                <div class="form-check d-inline-block">
                  <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
                </div>
                <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i> Practice</a>
                <p class="topic__progress-text">Explain mentor system</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
  </div>


  <!-- Chapter -->
  <div class="accordion" id="Subscribe-Course">
    <div class="card border-0">
      <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
        <a href="#" class="btn btn-accordion btn-block" type="button" data-toggle="collapse" data-target="#chapterOne"
          aria-expanded="true" aria-controls="chapterOne">
          <h3 class="section__title side-bar__subtitle">Chapter 1 - Basic Concept
            <span class="side-bar__subtitle--small">0/18 Complete</span>
          </h3>
          <span class="btn-accordion__icon"></span>
        </a>
      </div>
      <div id="chapterOne" class="collapse show" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
        <div class="accordion" id="chapterOne-topic">
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterOne-topicOne" aria-expanded="true"
            aria-controls="chapterOne-topicOne">
            <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
          </button>
          <div id="chapterOne-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterOne-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterOne-topicTwo" aria-expanded="true"
            aria-controls="chapterOne-topicTwo">
            <h4 class="section__title--top topic__title">Topic 2 - Coding Practice</h4>

          </button>
          <div id="chapterOne-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterOne-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterOne-topicThree" aria-expanded="true"
            aria-controls="chapterOne-topicThree">
            <h4 class="section__title--top topic__title">Topic 3 - IT Business Literacy</h4>

          </button>
          <div id="chapterOne-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterOne-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterOne-topicFour" aria-expanded="true"
            aria-controls="chapterOne-topicFour">
            <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>
          </button>
          <div id="chapterOne-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterOne-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-graduation-cap mr-2"></i>
                Exam</a>
              <p class="topic__progress-text">Final Exam</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <!-- Chapter 2 -->
    <div class="card border-0">
      <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
        <a href="#" class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
          data-target="#chapterTwo" aria-expanded="true" aria-controls="chapterTwo">
          <h3 class="section__title side-bar__subtitle">Chapter 2 - Basic HTML and CSS
            <span class="side-bar__subtitle--small">0/18 Complete</span>
          </h3>
          <span class="btn-accordion__icon"></span>
        </a>
      </div>
      <div id="chapterTwo" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
        <div class="accordion" id="chapterTwo-topic">
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterTwo-topicOne" aria-expanded="true"
            aria-controls="chapterTwo-topicOne">
            <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>

          </button>
          <div id="chapterTwo-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterTwo-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterTwo-topicTwo" aria-expanded="true"
            aria-controls="chapterTwo-topicTwo">
            <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>

          </button>
          <div id="chapterTwo-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterTwo-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterTwo-topicThree" aria-expanded="true"
            aria-controls="chapterTwo-topicThree">
            <h4 class="section__title--top topic__title">Topic 3 - Division</h4>

          </button>
          <div id="chapterTwo-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterTwo-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterTwo-topicFour" aria-expanded="true"
            aria-controls="chapterTwo-topicFour">
            <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>

          </button>
          <div id="chapterTwo-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterTwo-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-graduation-cap mr-2"></i>
                Exam</a>
              <p class="topic__progress-text">Final Exam</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <!-- Chapter 3 -->
    <div class="card border-0">
      <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
        <a href="#" class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
          data-target="#chapterThree" aria-expanded="true" aria-controls="chapterThree">
          <h3 class="section__title side-bar__subtitle">Chapter 3 - HTML & CSS Level Up
            <span class="side-bar__subtitle--small">0/18 Complete</span>
          </h3>
          <span class="btn-accordion__icon"></span>
        </a>
      </div>
      <div id="chapterThree" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
        <div class="accordion" id="chapterThree-topic">
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterThree-topicOne" aria-expanded="true"
            aria-controls="chapterThree-topicOne">
            <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>

          </button>
          <div id="chapterThree-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterThree-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterThree-topicTwo" aria-expanded="true"
            aria-controls="chapterThree-topicTwo">
            <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>

          </button>
          <div id="chapterThree-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterThree-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterThree-topicThree" aria-expanded="true"
            aria-controls="chapterThree-topicThree">
            <h4 class="section__title--top topic__title">Topic 3 - Division</h4>

          </button>
          <div id="chapterThree-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterThree-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterThree-topicFour" aria-expanded="true"
            aria-controls="chapterThree-topicFour">
            <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>

          </button>
          <div id="chapterThree-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterThree-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-graduation-cap mr-2"></i>
                Exam</a>
              <p class="topic__progress-text">Final Exam</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <!-- Chapter 4 -->
    <div class="card border-0">
      <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
        <a href="#" class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
          data-target="#chapterFour" aria-expanded="true" aria-controls="chapterFour">
          <h3 class="section__title side-bar__subtitle">Chapter 4 - Javascript
            <span class="side-bar__subtitle--small">0/18 Complete</span>
          </h3>
          <span class="btn-accordion__icon"></span>
        </a>
      </div>
      <div id="chapterFour" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
        <div class="accordion" id="chapterFour-topic">
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterFour-topicOne" aria-expanded="true"
            aria-controls="chapterFour-topicOne">
            <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>

          </button>
          <div id="chapterFour-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterFour-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterFour-topicTwo" aria-expanded="true"
            aria-controls="chapterFour-topicTwo">
            <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>

          </button>
          <div id="chapterFour-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterFour-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterFour-topicThree" aria-expanded="true"
            aria-controls="chapterFour-topicThree">
            <h4 class="section__title--top topic__title">Topic 3 - Division</h4>

          </button>
          <div id="chapterFour-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterFour-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterFour-topicFour" aria-expanded="true"
            aria-controls="chapterFour-topicFour">
            <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>

          </button>
          <div id="chapterFour-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterFour-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-graduation-cap mr-2"></i>
                Exam</a>
              <p class="topic__progress-text">Final Exam</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <!-- Chapter 5 -->
    <div class="card border-0">
      <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
        <a href="#" class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
          data-target="#chapterFive" aria-expanded="true" aria-controls="chapterFive">
          <h3 class="section__title side-bar__subtitle">Chapter 5 - Vue JS
            <span class="side-bar__subtitle--small">0/18 Complete</span>
          </h3>
          <span class="btn-accordion__icon"></span>
        </a>
      </div>
      <div id="chapterFive" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
        <div class="accordion" id="chapterFive-topic">
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterFive-topicOne" aria-expanded="true"
            aria-controls="chapterFive-topicOne">
            <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>

          </button>
          <div id="chapterFive-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterFive-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterFive-topicTwo" aria-expanded="true"
            aria-controls="chapterFive-topicTwo">
            <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>

          </button>
          <div id="chapterFive-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterFive-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterFive-topicThree" aria-expanded="true"
            aria-controls="chapterFive-topicThree">
            <h4 class="section__title--top topic__title">Topic 3 - Division</h4>

          </button>
          <div id="chapterFive-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterFive-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Paragraph
                <span class="topic__progress-text--small">15 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Heading
                <span class="topic__progress-text--small">7 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Bold, Italic, Underline</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 1</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Color, Font Family
                <span class="topic__progress-text--small">10 min 12 sec</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 2</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-play mr-2"></i>
                Video</a>
              <p class="topic__progress-text">Unordered List
                <span class="topic__progress-text--small">12 min</span>
              </p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="far fa-clone mr-2"></i>
                Slide</a>
              <p class="topic__progress-text">Ordered List</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Practice 3</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-code mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Coding Submision</p>
            </div>
            <div class="topic-inner__detail my-2">
              <div class="form-check d-inline-block">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-puzzle-piece mr-2"></i>
                Practice</a>
              <p class="topic__progress-text">Showing Text Content</p>
            </div>
          </div>
          <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
            data-toggle="collapse" data-target="#chapterFive-topicFour" aria-expanded="true"
            aria-controls="chapterFive-topicFour">
            <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>

          </button>
          <div id="chapterFive-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
            data-parent="#chapterFive-topic">
            <div class="topic-inner__detail my-2">
              <div class="form-check">
                <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox" value="option1">
              </div>
              <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                  class="fas fa-graduation-cap mr-2"></i>
                Exam</a>
              <p class="topic__progress-text">Final Exam</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>