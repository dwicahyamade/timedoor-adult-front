<div class="editor__sidebar">
  <div class="editor__sidebar-head">
    Directory
  </div>
  <p class="editor__sidebar-subtitle">
    <i class="fas fa-code"></i> index.html
  </p>
  <div class="editor__sidebar-content sidebar-content">
    <div class="accordion" id="accordionAssets">
      <div id="headingAssets">
        <h2 class="mb-0">
          <button class="btn sidebar-content__folder" type="button" data-toggle="collapse" data-target="#collapseAssets"
            aria-expanded="true" aria-controls="collapseAssets">
            <i class="far fa-folder folder-open mr-1"></i>
            <i class="fas fa-folder folder-close d-none mr-1"></i> Assets
          </button>
        </h2>
      </div>
      <div id="collapseAssets" class="collapse show" aria-labelledby="headingAssets" data-parent="#accordionAssets">
        <div class="sidebar-content__files">
          <i class="far fa-image mr-1"></i> Image7364.jpg
        </div>
        <div class="sidebar-content__files">
          <i class="far fa-image mr-1"></i> Image7364.jpg
        </div>
        <div class="sidebar-content__files">
          <i class="far fa-image mr-1"></i> Image7364.jpg
        </div>
        <!-- Subfolder -->
        <div class="accordion" id="accordionAssets-subOne">
          <div id="headingOne-subOne">
            <button class="btn sidebar-content__subfolder collapsed" type="button" data-toggle="collapse"
              data-target="#collapseOne-subOne" aria-expanded="false" aria-controls="collapseOne-subOne">
              <i class="fas fa-folder folder-close d-none mr-1"></i>
              <i class="far fa-folder folder-open mr-1"></i> js
            </button>
          </div>
          <div id="collapseOne-subOne" class="collapse" aria-labelledby="headingTwo-subOne"
            data-parent="#accordionAssets-subOne">
            <div class="sidebar-content__files--sub">
              <i class="fab fa-js-square mr-1"></i> script.js
            </div>
          </div>
        </div>

        <div class="accordion" id="accordionAssets-subTwo">
          <div id="headingOne-subTwo">
            <button class="btn sidebar-content__subfolder collapsed" type="button" data-toggle="collapse"
              data-target="#collapseOne-subTwo" aria-expanded="false" aria-controls="collapseOne-subTwo">
              <i class="fas fa-folder folder-close d-none mr-1"></i>
              <i class="far fa-folder folder-open mr-1"></i> css
            </button>
          </div>
          <div id="collapseOne-subTwo" class="collapse" aria-labelledby="headingTwo-subTwo"
            data-parent="#accordionAssets-subTwo">
            <div class="sidebar-content__files--sub">
              <i class="fab fa-css3-alt mr-1"></i> style.css
            </div>
          </div>
        </div>
        <!-- End subfolder -->
      </div>
    </div>
    <div class="accordion" id="accordionPages">
      <div id="headingPages">
        <h2 class="mb-0">
          <button class="btn sidebar-content__folder" type="button" data-toggle="collapse" data-target="#collapsePages"
            aria-expanded="false" aria-controls="collapsePages">
            <i class="far fa-folder folder-open mr-1"></i>
            <i class="fas fa-folder folder-close d-none mr-1"></i> Pages
          </button>
        </h2>
      </div>
      <div id="collapsePages" class="collapse show" aria-labelledby="headingPages-sub" data-parent="#accordionPages">
        <div class="sidebar-content__files active">
          <i class="fas fa-code mr-1"></i> index.html
        </div>
        <!-- Subfolder -->
        <div class="accordion" id="accordionPages-sub">
          <div id="headingPages-sub">
            <button class="btn sidebar-content__subfolder collapsed" type="button" data-toggle="collapse"
              data-target="#collapsePages-sub" aria-expanded="false" aria-controls="collapsePages-sub">
              <i class="fas fa-folder folder-close d-none mr-1"></i>
              <i class="far fa-folder folder-open mr-1"></i> product
            </button>
          </div>
          <div id="collapsePages-sub" class="collapse" aria-labelledby="headingPages-sub"
            data-parent="#accordionPages-sub">
            <div class="sidebar-content__files--sub">
              <i class="fas fa-code mr-1"></i> shoes.html
            </div>
          </div>
        </div>
        <!-- End subfolder -->
      </div>
    </div>
  </div>
  <div class="editor__sidebar-footer">
    <button class="bg-transparent border-0">
      <img src="/img/icon/icon-insert-pic.svg" alt="Insert New Picture" title="Upload Image">
    </button>
    <button class="bg-transparent border-0">
      <img src="/img/icon/icon-new-file.svg" alt="New File" title="New File">
    </button>
    <button class="bg-transparent border-0">
      <img src="/img/icon/icon-new-folder.svg" alt="New Folder" title="New Folder">
    </button>
  </div>
  <div class="editor__sidebar-btn"><i class="fas fa-angle-left arrow-left"></i><i
      class="fas fa-angle-right arrow-right d-none"></i></div>
</div>