<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="robots" content="noindex, nofollow" />

	<!-- Fonts Google -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

	<!-- UNIVERSAL CSS -->
	<link rel="stylesheet" href="css/layout.css">

	<!--  THIS PAGE ONLY CSS -->
	<link rel="stylesheet" href="css/pages/landing-page.css">

	<!-- Faveicon -->
	<link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

	<title>Landing Page - Timedoor Coding Academy</title>
</head>

<body class="landing-bg">

	<header class="nav-custom">
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container container-custom">
				<a class="navbar-brand" href="dashboard.php"><img src="img/timedoor-logo-black.png" alt="Timedoor Logo" width="141" height="33"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav ml-auto mt-4 mt-lg-0">
						<li class="nav-item mx-2 mb-4 mb-lg-0 order-2 order-lg-1">
							<a href="sign-up.php" class="btn btn-dark btn-start-free-trial mr-2 w-100" type="submit">Start Free Trial</a>
						</li>
						<li class="nav-item mx-2 mb-4 mb-lg-0 order-2 order-lg-1">
							<a href="index.php" class="btn btn-outline-dark h-100 w-100" type="submit">Log In</a>
						</li>
						<div class="nav-custom__language-mobile d-flex d-lg-none">
							<a href="#" class="nav-custom__flag">
								<img src="img/indonesian-flag.svg" alt="Bahasa Indonesia">
							</a>
							|
							<a href="#" class="nav-custom__flag">
								<img src="img/united-kingdom-flag.svg" alt="English">
							</a>
						</div>
						<li class="d-none d-lg-block nav-item my-4 my-lg-0 order-1 order-lg-1 text-right text-lg-center">
							<div class="btn-group btn-language h-100">
								<button type="button" class="btn btn-transparent px-0 px-lg-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									English
									<i class="fas fa-chevron-down fa-sm arrow-down"></i>
								</button>
								<div class="dropdown-menu dropdown-menu-right dropdown-custom-nav position-absolute">
									<a class="dropdown-item dropdown-custom-nav__item" href="">English</a>
									<a class="dropdown-item dropdown-custom-nav__item" href="">Indonesia</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<main>

		<section class="top-hero">
			<div class="container container-custom">
				<div class="row justify-content-between">
					<div class="top-hero__image">
						<picture>
							<source media="(max-width:425px)" srcset="img/landing-page/hero-image-mobile.svg">
							<img src="img/landing-page/hero-image-big.svg" alt="Ayo Gabung & Belajar Coding Kapan Saja Dimana Saja" width="100%">
						</picture>
					</div>
					<div class="col-xl-6 col-lg-5 top-hero__text-container ml-auto">
						<h1 class="top-hero__title">Ayo Gabung & Belajar Coding Kapan Saja Dimana Saja
						</h1>
						<div class="top-hero__desc-container">
							<p class="top-hero__desc">
								Ayo kembangkan kemampuanmu dengan waktu belajar yang fleksibel dan bisa dikerjakan dimana saja.
							</p>
							<a href="#" class="btn btn-green mt-4">Coba Trial Gratis</a>
						</div>
						<div class="top-hero__icon-container">
							<div class="top-hero__icon-wrapper">
								<img src="img/landing-page/icon-hero-topic.svg" alt="Topic">
								<span class="top-hero__icon-text mt-2">100+</span>
								<span class="top-hero__icon-text--small">Topik</span>
							</div>
							<div class="top-hero__icon-wrapper">
								<img src="img/landing-page/icon-hero-online.svg" alt="Online">
								<span class="top-hero__icon-text mt-2">100%</span>
								<span class="top-hero__icon-text--small">Online</span>
							</div>
							<div class="top-hero__icon-wrapper">
								<img src="img/landing-page/icon-hero-project.svg" alt="Project">
								<span class="top-hero__icon-text mt-2">Project</span>
								<span class="top-hero__icon-text--small">Siap pakai</span>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section>

		<section class="section online-course">

			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="section__title online-course__title">Online Course Terbaik Untukmu</h2>
					</div>
					<div class="col-12 col-lg-7">
						<p class="section__desc online-course__desc">Dengan berlangganan salah satu course di Timedoor Pro Coder kamu akan mendapatkan banyak manfaat</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="online-course__icon">
							<img src="img/landing-page/icon-time.svg" alt="Flexible Time">
						</div>
						<h3 class="online-course__icon-title">Waktu Yang Flexible</h3>
						<p class="online-course__icon-desc">Belajar course pilihanmu kapan saja dan dimana saja</p>
					</div>
					<div class="col-lg-4">
						<div class="online-course__icon">
							<img src="img/landing-page/icon-skill.svg" alt="Flexible Time">
						</div>
						<h3 class="online-course__icon-title">Dapatkan Skill Programming</h3>
						<p class="online-course__icon-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
					<div class="col-lg-4">
						<div class="online-course__icon">
							<img src="img/landing-page/icon-affordable.svg" alt="Flexible Time">
						</div>
						<h3 class="online-course__icon-title">Affordable Untuk Pemula</h3>
						<p class="online-course__icon-desc">Dapatkan skill pemrograman dan rilis projek pribadimu dengan harga yang ekonomis</p>
					</div>
				</div>
			</div>

		</section>

		<section class="section feature">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<h2 class="section__title feature__title">Fitur Kami</h2>

						<div class="accordion feature__accordion" id="accordionFeature">

							<div class="feature__card">
								<div class="feature__card-header" id="headingOne">
									<h3 class="mb-0">
										<button id="feature-first" class="btn feature__accordion-title" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											Materi Berupa Slide & Video
											<span class="feature__accordion-icon"></span>
										</button>
									</h3>
								</div>
								<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFeature">
									<div class="feature__accordion-body">
										Timedoor Pro Coder menyediakan materi yang mudah dipahami dan dilengkapi dengan video tutorial
									</div>
									<img src="img/landing-page/feature-slideVideo.svg" alt="Materi Slide & Video" width="100%" class="d-block d-lg-none">
								</div>
							</div>

							<hr>

							<div class="feature__card">
								<div class="feature__card-header" id="headingTwo">
									<h3 class="mb-0">
										<button id="feature-second" class="btn feature__accordion-title collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
											Berlatih Coding Dengan Code Editor
											<span class="feature__accordion-icon"></span>
										</button>
									</h3>
								</div>
								<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionFeature">
									<div class="feature__accordion-body">
										Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore nesciunt alias doloribus corporis odit vero!
									</div>
									<img src="img/landing-page/feature-codeEditor.svg" alt="Materi Slide & Video" width="100%" class="d-block d-lg-none">
								</div>
							</div>

							<hr>

							<div class="feature__card">
								<div class="feature__card-header" id="headingThree">
									<h3 class="mb-0">
										<button id="feature-third" class="btn feature__accordion-title collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
											Code Review Oleh Professional
											<span class="feature__accordion-icon"></span>
										</button>
									</h3>
								</div>
								<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionFeature">
									<div class="feature__accordion-body">
										Lorem, ipsum dolor sit amet consectetur adipisicing elit. Esse laudantium ducimus atque!
									</div>
									<img src="img/landing-page/feature-reviewProfesional.svg" alt="Materi Slide & Video" width="100%" class="d-block d-lg-none">
								</div>
							</div>

							<hr>

							<div class="feature__card">
								<div class="feature__card-header" id="headingFour">
									<h3 class="mb-0">
										<button id="feature-fourth" class="btn feature__accordion-title collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
											Kuis Seputar Materi
											<span class="feature__accordion-icon"></span>
										</button>
									</h3>
								</div>
								<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionFeature">
									<div class="feature__accordion-body">
										Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui voluptatem facere doloremque dolorem doloribus!
									</div>
									<img src="img/landing-page/feature-quiz.svg" alt="Materi Slide & Video" width="100%" class="d-block d-lg-none">
								</div>
							</div>

							<hr>

							<div class="feature__card">
								<div class="feature__card-header" id="headingFive">
									<h3 class="mb-0">
										<button id="feature-fifth" class="btn feature__accordion-title collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
											Online Mentoring Oleh Profesional
											<span class="feature__accordion-icon"></span>
										</button>
									</h3>
								</div>
								<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionFeature">
									<div class="feature__accordion-body">
										Lorem, ipsum dolor sit amet consectetur adipisicing elit. Esse laudantium ducimus atque!
									</div>
									<img src="img/landing-page/feature-onlineMentoring.svg" alt="Materi Slide & Video" width="100%" class="d-block d-lg-none">
								</div>
							</div>

						</div>

					</div>
					<div class="col-lg-6">
						<div class="feature__image-container">
							<img src="img/landing-page/feature-slideVideo.svg" id="feature__image" alt="Materi Slide & Video" width="100%" class="d-none d-lg-block">
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section curriculum">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 d-none d-lg-block">
						<div class="curriculum__image">
							<img src="img/landing-page/curriculum-1.svg" alt="Kurikulum Kami" width="90%">
						</div>
					</div>
					<div class="col-lg-6">
						<h2 class="section__title curriculum__title">Kurikulum Kami</h2>
						<p class="section__desc curriculum__desc">Kurikulum Timedoor Pro Coder dibuat berdasarkan kebutuhan siswa. Agar setelah mengambil course, siswa dapat langsung membuat project sendiri.</p>
						<div class="curriculum__image">
							<img src="img/landing-page/curriculum-2.svg" alt="Kurikulum Kami" width="100%">
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section why-important">
			<div class="container">
				<div class="row justify-content-end">
					<div class="col-12 col-lg-6">
						<h2 class="section__title why-important__title">Mengapa Belajar Coding sangat Penting?</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 order-1 order-lg-0 mt-n5 d-none d-lg-block">
						<div class="why-important__image">
							<img src="img/landing-page/why-important-image.svg" alt="Mengapa Belajar Coding sangat Penting?" width="90%">
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-12">
								<p class="section__desc why-important__desc">Saat ini teknologi semakin canggih, dan kebanyakan teknologi dibangun dengan pemrograman. Dengan mempelajari Coding kamu akan mendapatkan banyak hal. Jika menguasai Coding dengan baik, semua kamudahan kamu akan semakin dibutuhkan dalam dunia pekerjaan.</p>
							</div>
							<div class="col-12 text-center d-block d-lg-none">
								<img src="img/landing-page/why-important-image.svg" alt="Mengapa Belajar Coding sangat Penting?" width="90%">
							</div>
							<div class="col-12">
								<ul class="why-important__ul">
									<li class="why-important__li"><img src="img/icon/icon-check-green.svg" alt="checkmark" class="mr-3"> Semua orang butuh kemampuan di bidang teknologi</li>
									<li class="why-important__li"><img src="img/icon/icon-check-green.svg" alt="checkmark" class="mr-3"> Dunia membutuhkan lebih banyak Programmer</li>
									<li class="why-important__li"><img src="img/icon/icon-check-green.svg" alt="checkmark" class="mr-3"> Programmer memiliki rata - rata gaji yang tinggi</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section mentor">
			<div class="container">
				<div class="row">
					<div class="col-lg-5">
						<h2 class="section__title mentor__title">Belajar Coding Bersama Mentor Profesional</h2>
						<p class="section__desc mentor__desc">Mentor Timedoor Pro Code sudah berpengalaman di bidang teknologi khususnya programming. Jadi jika kamu mengalami kesulitan selama belajar atau saat membuat project pribadimu, jangan ragu untuk bertanya pada mentor. Mentor akan sigap membantu menyelesaikan permasalah yang ada.</p>
					</div>
					<div class="col-lg-7">
						<div class="mentor__image">
							<img src="img/landing-page/learn-with-mentor.svg" alt="Belajar Coding Bersama Mentor Profesional" width="90%">
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section">
			<div class="our-story">
				<div class="container">
					<div class="row justify-content-between">
						<div class="col-lg-4">
							<h2 class="section__title our-story__title">Cerita Sukses Siswa Kami</h2>
						</div>
						<div class="col-lg-5">
							<p class="section__desc our-story__desc">Ayo ketahui keberhasilan dan pengalaman para alumni siswa Timedoor Pro Coder selama belajar mandiri dengan bantuan mentor</p>
						</div>
					</div>
				</div>
				<!-- Swiper -->
				<div class="swiper mySwiper">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<div class="our-story__slide">
								<div class="our-story__image mb-3 mb-lg-0">
									<img src="img/landing-page/our-story-1.svg" alt="Our Story" width="122">
									<div class="text-center d-block d-lg-none mt-3">
										<div class="our-story__name">Ahmad/30th</div>
										<div class="our-story__job">Software Engineer Android</div>
									</div>
								</div>
								<div class="our-story__text">
									<div class="our-story__story">“Saya khusus mendedikasikan waktu saya untuk belajar ngoding. Di Timedoor Academy Adult belajarnya step by step, library-nya up-to-date. Kalau ada eror, nggak bingung."</div>
									<div class="d-none d-lg-block">
										<div class="our-story__name">Ahmad/30th</div>
										<div class="our-story__job">Software Engineer Android</div>
									</div>
								</div>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="our-story__slide">
							<div class="our-story__image mb-3 mb-lg-0">
									<img src="img/landing-page/our-story-2.svg" alt="Our Story" width="122">
									<div class="text-center d-block d-lg-none mt-3">
										<div class="our-story__name">Evelyn/28th</div>
										<div class="our-story__job">Programmer</div>
									</div>
								</div>
								<div class="our-story__text">
									<div class="our-story__story">“Saya khusus mendedikasikan waktu saya untuk belajar ngoding. Di Timedoor Academy Adult belajarnya step by step, library-nya up-to-date. Kalau ada eror, nggak bingung."</div>
									<div class="d-none d-lg-block">
										<div class="our-story__name">Evelyn/28th</div>
										<div class="our-story__job">Programmer</div>
									</div>
								</div>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="our-story__slide">
							<div class="our-story__image mb-3 mb-lg-0">
									<img src="img/landing-page/our-story-1.svg" alt="Our Story" width="122">
									<div class="text-center d-block d-lg-none mt-3">
										<div class="our-story__name">Ahmad/30th</div>
										<div class="our-story__job">Software Engineer Android</div>
									</div>
								</div>
								<div class="our-story__text">
									<div class="our-story__story">“Saya khusus mendedikasikan waktu saya untuk belajar ngoding. Di Timedoor Academy Adult belajarnya step by step, library-nya up-to-date. Kalau ada eror, nggak bingung."</div>
									<div class="d-none d-lg-block">
										<div class="our-story__name">Ahmad/30th</div>
										<div class="our-story__job">Software Engineer Android</div>
									</div>
								</div>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="our-story__slide">
							<div class="our-story__image mb-3 mb-lg-0">
									<img src="img/landing-page/our-story-2.svg" alt="Our Story" width="122">
									<div class="text-center d-block d-lg-none mt-3">
										<div class="our-story__name">Evelyn/28th</div>
										<div class="our-story__job">Programmer</div>
									</div>
								</div>
								<div class="our-story__text">
									<div class="our-story__story">“Saya khusus mendedikasikan waktu saya untuk belajar ngoding. Di Timedoor Academy Adult belajarnya step by step, library-nya up-to-date. Kalau ada eror, nggak bingung."</div>
									<div class="d-none d-lg-block">
										<div class="our-story__name">Evelyn/28th</div>
										<div class="our-story__job">Programmer</div>
									</div>
								</div>
							</div>
						</div>
						<div class="swiper-slide">
							<div class="our-story__slide">
								<div class="our-story__image mb-3 mb-lg-0">
									<img src="img/landing-page/our-story-1.svg" alt="Our Story" width="122">
									<div class="text-center d-block d-lg-none mt-3">
										<div class="our-story__name">Ahmad/30th</div>
										<div class="our-story__job">Software Engineer Android</div>
									</div>
								</div>
								<div class="our-story__text">
									<div class="our-story__story">“Saya khusus mendedikasikan waktu saya untuk belajar ngoding. Di Timedoor Academy Adult belajarnya step by step, library-nya up-to-date. Kalau ada eror, nggak bingung."</div>
									<div class="d-none d-lg-block">
										<div class="our-story__name">Ahmad/30th</div>
										<div class="our-story__job">Software Engineer Android</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-pagination our-story__pagination"></div>
			</div>
		</section>

		<section class="section payment">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<h2 class="section__title payment__title">Tunggu Apa Lagi? <br> Ayo mulai sekarang juga!</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 mb-3 mb-lg-0 order-2 order-lg-1">
						<div class="row">
							<div class="col-12">
								<p class="section__desc payment__desc">Silahkan uji kemampuan dan minatmu dengan mencoba trial gratis dari kami. Bila kamu sudah yakin, ambil course ini segera!</p>
							</div>
							<div class="col-12">
								<div class="payment__price-container">
									<img src="img/landing-page/icon-best-price.png" alt="Best Price" class="payment__best-deal">
									<h3 class="payment__price">IDR 1,000,<span class="payment__price--small">000,-</span><span class="payment__price--month"> /4 Bulan</span>
									</h3>
								</div>
							</div>
						
						<!-- <h3 class="payment__price"><img src="img/landing-page/icon-best-price.png" alt="Best Price"> IDR 1,000,<span class="payment__price--small">000,-/ 4 Bulan</span></h3> -->
							<div class="col-12 order-2 order-lg-1">
								<div class="row">
									<div class="col-sm-6 mb-3 mb-lg-0">
										<a href="#" class="btn payment__btn">Bayar Sekarang</a>
									</div>
									<div class="col-sm-6">
										<a href="#" class="btn payment__btn--border">Coba Trial Gratis</a>
									</div>
								</div>
							</div>
							<div class="col-12 order-1 order-lg-1">
								<p class="payment__metode">Metode Pembayaran : <img src="img/landing-page/midtrans-logo.svg" alt="Midtrans" class="ml-2"></p>
							</div>

						</div>
					</div>
					<div class="col-lg-6 order-1 order-lg-1 mb-4 mb-lg-0">
						<div class="payment__image-container text-center text-lg-right">
							<img src="img/landing-page/payment.svg" alt="Payment" class="payment__image" width="80%">
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="section contact">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-lg-3 my-1">
						<img src="img/timedoor-logo-black.png" alt="Timedoor" width="184" height="auto">
					</div>
					<div class="col-sm-6 col-lg-3 my-1">
						<a href="mailto:info@timedoor.net" class="contact__text"><img src="img/landing-page/icon-mail.svg" alt="Mail Us" class="mr-2"> info@timedoor.net</a>
					</div>
					<div class="col-lg-6 my-1">
						<p class="contact__text"><img src="img/landing-page/icon-location.svg" alt="Location" class="mr-2"> Jl. Tukad Yeh Aya IX No.46, Renon , Denpasar, Bali</p>
					</div>
				</div>
			</div>
		</section>

		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-7">
						<p class="footer__text">Timedoor Coding Academy Adult ©2021 PT. Timedoor Indonesia All Right Reserved</p>
					</div>
					<div class="col-lg-5 text-center text-lg-right">
						<a href="privacy-terms.php" class="footer__text">Privacy policy</a>
						<span class="footer__text">|</span>
						<a href="privacy-terms.php" class="footer__text">Terms of service</a>
					</div>
				</div>
			</div>
		</footer>

	</main>

	<!-- Bootstrap -->
	<script src="js/jquery-3.5.1.slim.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>

	<!-- Swiperjs -->
	<script src="js/swiper.js"></script>
	<script src="js/pages/landing-page.js"></script>

</body>

</html>