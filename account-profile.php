<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/account.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Account Info - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "component/header.php" ?>

  <main>
    <section class="account">
      <div class="container">
        <h2 class="section__title">Your Profile</h2>
        <div class="row">
          <div class="col-lg-8">
            <div class="account__left">
              <div class="row justify-content-between">
                <div class="col-3">
                  <img src="img/user-default-white.png" alt="User" class="img-thumbnail account-info__image">
                </div>
                <div class="col-8">
                  <h1 class="section__title account-info__title">Setyo Syahindra</h1>

                  <p class="account__desc--small">
                    <img src="img/icon/icon-birthday.svg" alt="Birthday" class="mr-2 align-text-top">12 Dec / Male
                  </p>

                  <p class="account__desc"><i class="far fa-envelope mr-2"></i>setyosyahindra100@gmail.com</p>
                  <p class="account__desc"><img src="img/icon/icon-tel.svg" alt="Phone" class="mr-2">+62 87 335 445 888
                  </p>
                  <p class="account__desc"><img src="img/icon/icon-location.svg" alt="province" class="mr-2">Denpasar,Bali</p>
                  <a href="account-info.php" class="btn btn-dark mt-5 w-75">Edit Profile</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 mt-3 mt-lg-0 pl-lg-0">
            <div class="account__right">
              <h3 class="section__title--top mb-1">
                Subscription Status
              </h3>
              <span class="account__indicator">Active</span>
              <div class="row mt-5">
                <div class="col-md-6 border-right">
                  <div class="d-block account__subtitle">Joined</div>
                  <div class="d-block account__date">10-10-2021</div>
                  <small class="account__time">10.23 PM</small>
                </div>
                <div class="col-md-6">
                  <div class="d-block account__subtitle">Last Learning</div>
                  <div class="d-block account__date">10-10-2021</div>
                  <small class="account__time">07.23 PM</small>
                </div>
              </div>
              <p class="account__subtitle mb-0 mt-5">Last Learning Progress</p>
              <p class="account__subtitle--small">Build a Website With HTML & CSS</p>
              <div class="progress progress-custom">
                <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                <span class="progress-custom__percent">50%</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Button Whatsapp -->
    <?php require_once 'component/button-whatsapp.php' ?>

  </main>

  <?php require_once "component/footer.php" ?>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>