const sideBarBtn = document.querySelector('.side-bar__btn-show');
const sidebarMenu = document.querySelector('.side-bar');
const sidebarMenuClose = document.querySelector('.side-bar__btn-close');

sideBarBtn.addEventListener('click', function () {
  sidebarMenu.classList.toggle('show');
})

sidebarMenuClose.addEventListener('click', function () {
  sidebarMenu.classList.remove('show');
})