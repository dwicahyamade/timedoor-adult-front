const inputPassword = document.getElementById('password');
const iconPassword = document.querySelector('.input-password__icon');

inputPassword.addEventListener('focus', function () {
  iconPassword.classList.add('show')
})

inputPassword.addEventListener('blur', function () {
  iconPassword.classList.remove('show')
})

iconPassword.addEventListener('click', function () {
  if (inputPassword.type === "password") {
    inputPassword.type = "text";
  } else {
    inputPassword.type = "password"
  }
})