const btnscrollDown = document.getElementById("btn-down");
const btnscrollUp = document.getElementById("btn-up");
const btnscrollScroll = document.getElementById("btn-scroll");

var lastSlide = document.querySelector(".slide-second__bg").lastElementChild;
var firstSlide = document.querySelector(".slide-second__bg").firstElementChild;

window.onload = function () {
  if (lastSlide.classList.contains("active")) {
    btnscrollDown.style.display = "none";
  }

  if (firstSlide.classList.contains("active")) {
    btnscrollUp.style.display = "none";
  }
};

btnscrollDown.addEventListener("click", function () {
  var currentSlide = document.querySelector(".slide-second__content.active");
  var nextSlide = currentSlide.nextElementSibling;
  if (nextSlide) {
    btnscrollUp.style.display = "block";
    currentSlide.classList.remove("active");
    currentSlide.classList.add("prev");
    // currentSlide.classList.remove("next");

    nextSlide.classList.add("active");
    nextSlide.classList.remove("next");
  }

  if (lastSlide.classList.contains("active")) {
    btnscrollDown.style.display = "none";
  }

  // console.log(lastSlide)
});

btnscrollUp.addEventListener("click", function () {
  var currentSlide = document.querySelector(".slide-second__content.active");
  var prevSlide = currentSlide.previousElementSibling;
  if (prevSlide) {
    btnscrollDown.style.display = "block";
    currentSlide.classList.remove("active");
    currentSlide.classList.add("next");

    prevSlide.classList.add("active");
    prevSlide.classList.remove("prev");
  }

  if (firstSlide.classList.contains("active")) {
    btnscrollUp.style.display = "none";
  }
});

// Scroll to slide
btnscrollScroll.addEventListener("click", function () {
  window.scrollTo(0, document.body.scrollHeight);
});
