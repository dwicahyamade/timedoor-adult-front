const backToTop = document.querySelector('.btn-back-to-top');

backToTop.addEventListener('click', function () {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
})