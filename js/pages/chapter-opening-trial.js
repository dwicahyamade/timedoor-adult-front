/* Get Variable for each button */
var nextBtn = document.querySelector('.chapter__next');
var prevBtn = document.querySelector('.chapter__prev');

/* get variable prev button and next button */
var firstContent = document.querySelector(".chapter__bg").firstElementChild;
var lastContent = document.querySelector(".chapter__bg").lastElementChild;

/* Remove prev button when first content */
if (firstContent.classList.contains("active")) {
  prevBtn.style.display = "none";
}

// if (lastContent.classList.contains("active")) {
//   // nextBtn.style.display = "none";
//   alert("ok")
// }

nextBtn.addEventListener('click', function(){
  
  var currentChapterContent = document.querySelector('.chapter__content.active');
  var nextChapterContent = currentChapterContent.nextElementSibling;

  /* Robot */
  var currentRobot = document.querySelector('.chapter__robot-image.active');
  var nextRobot = currentRobot.nextElementSibling;
  
  if (nextChapterContent){
    
    currentChapterContent.classList.remove('active');
    currentChapterContent.classList.add('hidden');
    currentChapterContent.nextElementSibling.classList.add('active');
    currentChapterContent.nextElementSibling.classList.remove('hidden');

    /* show prev button when next clicked */
    prevBtn.style.display = "inline-block";
    
    /* Change Robot image */
    currentRobot.classList.remove('active');
    currentRobot.classList.add('hidden');
    currentRobot.nextElementSibling.classList.add('active');
    currentRobot.nextElementSibling.classList.remove('hidden');
    
  }

  /* Remove prev button when first content */
  if (lastContent.classList.contains("active")) {
    nextBtn.style.display = "none";
  }
})

prevBtn.addEventListener('click', function(){
  
  var currentChapterContent = document.querySelector('.chapter__content.active');
  var prevChapterContent = currentChapterContent.previousElementSibling;

  /* Robot */
  var currentRobot = document.querySelector('.chapter__robot-image.active');
  var nextRobot = currentRobot.previousElementSibling;
  
  if (prevChapterContent){
    
    currentChapterContent.classList.remove('active');
    currentChapterContent.classList.add('hidden');
    currentChapterContent.previousElementSibling.classList.add('active');
    currentChapterContent.previousElementSibling.classList.remove('hidden');

    /* show next button when next clicked */
    nextBtn.style.display = "inline-block";

    /* Change Robot image */
    currentRobot.classList.remove('active');
    currentRobot.classList.add('hidden');
    currentRobot.previousElementSibling.classList.add('active');
    currentRobot.previousElementSibling.classList.remove('hidden');
  }

  /* Remove next button when last content */
  if (firstContent.classList.contains("active")) {
    prevBtn.style.display = "none";
  }

})