const updateBtn = document.querySelector('.btn-save');
const alertInfo = document.querySelector('.alert-custom');

updateBtn.addEventListener('click', function () {
  alertInfo.classList.add('show');
  setTimeout(function () {
    alertInfo.classList.remove('show');
  }, 3000)
})