const sidebarBtnEditor = document.querySelector('.editor__sidebar-btn');
const sidebarMenuEditor = document.querySelector('.editor__sidebar');

sidebarBtnEditor.addEventListener('click', function () {
  sidebarMenuEditor.classList.toggle('hide')
})


const btnBadge = document.getElementById('btn-badge');
const btnScoreClose = document.querySelector('.overlay__score-close');

const overlay = document.querySelector('.overlay');
const overlayScore = document.querySelector('.overlay__score');


if (btnBadge != null) {
  btnBadge.addEventListener('click', function () {
    overlay.classList.toggle('show');
    overlayScore.classList.toggle('show');
  })
}

if (btnScoreClose != null) {
  btnScoreClose.addEventListener('click', function () {
    overlay.classList.remove('show');
    overlayScore.classList.remove('show');
  })
}

// Ace Editor
var editor = ace.edit("editor");
editor.setTheme("ace/theme/tomorrow_night_bright");
editor.session.setMode("ace/mode/javascript");
editor.setOptions({
  fontSize: "18px"
});


jQuery(function ($) {
  $('.btn-requirement').on('click', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('collapsed')) {
      e.stopPropagation();
      return false;
    }
  });
});