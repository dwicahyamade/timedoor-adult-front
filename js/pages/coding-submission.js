const sidebarBtnEditor = document.querySelector('.editor__sidebar-btn');
const sidebarMenuEditor = document.querySelector('.editor__sidebar');

sidebarBtnEditor.addEventListener('click', function () {
  sidebarMenuEditor.classList.toggle('hide')
})


const btnCheck = document.querySelector('.btn-check');
const btnUpdate = document.querySelector('.btn-update');
const overlayDraftBtn = document.querySelector('.editor__btn-draft');

const overlay = document.querySelector('.overlay');
const overlayCheck = document.querySelector('.overlay__icon-check');
const overlayUpdate = document.querySelector('.overlay__icon-update');
const overlaySubmitted = document.querySelector('.overlay__submited');
const overlayDraft = document.querySelector('.overlay__draft');

if (btnCheck != null) {
  btnCheck.addEventListener('click', function () {
    overlay.classList.add('show');
    overlayCheck.classList.add('show');
    setTimeout(function () {
      overlay.classList.remove('show');
      overlayCheck.classList.remove('show');
    }, 2000)

    setTimeout(function () {
      overlay.classList.add('show');
      overlaySubmitted.classList.add('show');
    }, 2000)

    setTimeout(function () {
      overlay.classList.remove('show');
      overlaySubmitted.classList.remove('show');
    }, 4000)
  })
}

if (btnUpdate != null) {
  btnUpdate.addEventListener('click', function () {
    overlay.classList.add('show');
    overlayUpdate.classList.add('show');
    setTimeout(function () {
      overlay.classList.remove('show');
      overlayUpdate.classList.remove('show');
    }, 2000)
  })
}

// overlaySubmittedBtn.addEventListener('click', function () {
//   overlaySubmitted.classList.toggle('show');
// })

if (overlayDraftBtn != null) {
  overlayDraftBtn.addEventListener('click', function () {
    overlay.classList.add('show');
    overlayDraft.classList.add('show');
    setTimeout(function () {
      overlay.classList.remove('show');
      overlayDraft.classList.remove('show');
    }, 2000)
  })
}

// Ace Editor
var editor = ace.edit("editor");
editor.setTheme("ace/theme/tomorrow_night_bright");
editor.session.setMode("ace/mode/javascript");
editor.setOptions({
  fontSize: "18px"
});


jQuery(function ($) {
  $('.btn-requirement').on('click', function (e) {
    e.preventDefault();
    if (!$(this).hasClass('collapsed')) {
      e.stopPropagation();
      return false;
    }
  });
});