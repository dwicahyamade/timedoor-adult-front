var border = document.querySelectorAll('.accordion-border');
var topic = document.querySelectorAll('.btn-accordion-inner');


for (const topicItem of topic) {

  topicItem.addEventListener('click', function () {
    for (let i = 0; i < border.length; i++) {
      border[i].classList.remove('show')
    }
    if (topicItem.classList.contains("collapsed")) {
      topicItem.closest(".accordion-border").classList.add('show');
    }
  })
}


// Mentor Page
const arrowRight = document.querySelector('.arrow-slider__right')
const arrowLeft = document.querySelector('.arrow-slider__left')
const slider = document.querySelector('.categories__wrapper');

if (arrowRight != null) {
  arrowRight.addEventListener('click', function () {
    slider.classList.add('next');
    arrowRight.classList.add('disabled');
    arrowLeft.classList.remove('disabled');
  })
}

if (arrowLeft != null) {
  arrowLeft.addEventListener('click', function () {
    slider.classList.remove('next');
    arrowLeft.classList.add('disabled');
    arrowRight.classList.remove('disabled');
  })
}