const sidebarBtnEditor = document.querySelector('.editor__sidebar-btn');
const sidebarMenuEditor = document.querySelector('.editor__sidebar');

sidebarBtnEditor.addEventListener('click', function () {
  sidebarMenuEditor.classList.toggle('hide')
})

const btnCheck = document.querySelector('.btn-green');
const btnError = document.querySelector('.test-error');
const btnSuccess = document.querySelector('.test-correct');

const overlay = document.querySelector('.overlay');
const overlayError = document.querySelector('.overlay__error');
const overlaySuccess = document.querySelector('.overlay__success');
const overlayCheck = document.querySelector('.overlay__icon-check');

btnCheck.addEventListener('click', function () {
  overlay.classList.add('show');
  overlayCheck.classList.add('show');
  setTimeout(function () {
    overlay.classList.remove('show');
    overlayCheck.classList.remove('show');
  }, 3000)

  // setTimeout(function () {
  //   overlay.classList.remove('show');
  //   overlaySuccess.classList.remove('show');
  // }, 4000)
})

btnSuccess.addEventListener('click', function () {
  overlay.classList.toggle('show');
  overlaySuccess.classList.toggle('show');
})

btnError.addEventListener('click', function () {
  overlay.classList.toggle('show');
  overlayError.classList.toggle('show');
})

overlayCheck.addEventListener('click', function () {
  overlaySuccess.classList.toggle('show');
})

// ROBOT OVERLAY
const robotNextOverlay = document.querySelectorAll('.robot-overlay__next--small');
const robotPrevOverlay = document.querySelectorAll('.robot-overlay__prev--small');

// REMOVE CLASS ACTIVE TO NEXT BUTTON WHEN CLICKED
for (let i = 0; i < robotNextOverlay.length; i++){
  robotNextOverlay[i].addEventListener('click', function(){
    robotNextOverlay[i].classList.remove('active')
    // console.log('ok')
  })
}

// REMOVE CLASS ACTIVE TO PREV BUTTON WHEN CLICKED
for (let i = 0; i < robotPrevOverlay.length; i++){
  robotPrevOverlay[i].addEventListener('click', function(){
    robotPrevOverlay[i].classList.remove('active')
    // console.log('ok')
  })
}

// CLOSE ROBOT OVERLAY WHEN FINISH BUTTON CLICKED
// GET BUTTON
const robotFinish = document.querySelector('.robot-overlay__finish');
const robotClose = document.querySelectorAll('.robot-overlay__close');
const robotOverlay = document.querySelector('.robot-overlay');

// CLOSE ROBOT OVERLAY WHEN FINISH BUTTON CLICKED
robotFinish.addEventListener('click', function(){
  robotOverlay.classList.add('hidden');
})

// CLOSE ROBOT OVERLAY WHEN X BUTTON CLICKED
for (i = 0; i < robotClose.length; i++){
  robotClose[i].addEventListener('click', function(){
    robotOverlay.classList.add('hidden');
  })
}

// First Robot TAB
  // GET ELEMENT
  let codingLeft = document.getElementById('coding-left');
  let robotCloneFirst = document.querySelector('.robot-overlay__clone.first');

  // CLONE ELEMENT
  let cloneFirst = codingLeft.cloneNode(true);
  let cloneWidthFirst = codingLeft.offsetWidth;

  // PLACE ELEMENT
  robotCloneFirst.appendChild(cloneFirst);
  cloneFirst.style.width = cloneWidthFirst + "px";
  // ADD CLASS HIGHLIGHT
  cloneFirst.classList.add('highlight');
// FIrst Robot TAB END]

// Second Robot TAB
  // add class highlight when button clicked
  document.querySelector('[href="#robot-second"]').addEventListener('click', function(){
    document.querySelector('.progress-bar-custom').classList.add('highlight')
  })
// Second Robot TAB END

// Third Robot TAB
  document.querySelector('[href="#robot-third"]').addEventListener('click', function(){
    // remove class highlight when button clicked
    document.querySelector('.progress-bar-custom').classList.remove('highlight')
    // add class highlight when button clicked
    document.querySelector('.side-bar__btn-show').classList.add('highlight')
  })
// Third Robot TAB END

// Fourth Robot TAB
  // add class highlight when button clicked
  document.querySelector('[href="#robot-fourth"]').addEventListener('click', function(){
    document.querySelector('.side-bar__btn-show').classList.remove('highlight')
  })
// Fourth Robot TAB END

// Fourth Robot TAB
  // GET ELEMENT
  let editorSidebar = document.querySelector('.editor__sidebar');
  let robotCloneFourth = document.querySelector('.robot-overlay__clone.fourth');
  let editorSidebarPosition = editorSidebar.getBoundingClientRect();

  // CLONE ELEMENT
  let cloneFourth = editorSidebar.cloneNode(true);
  let cloneWidthFourth = editorSidebar.offsetWidth;

  // PLACE ELEMENT
  robotCloneFourth.appendChild(cloneFourth);
  cloneFourth.style.width = cloneWidthFourth + "px";
  cloneFourth.style.left = editorSidebarPosition.left + "px";

  // ADD CLASS HIGHLIGHT
  cloneFourth.classList.add('highlight');
// FIrst Robot TAB END]

// Fifth Robot TAB
  // GET ELEMENT
  let robotCloneFifth = document.querySelector('.robot-overlay__clone.fifth');
  let editorSidebarFooterPosition = editorSidebar.getBoundingClientRect();

  // CLONE ELEMENT
  let cloneFifth = editorSidebar.cloneNode(true);
  let cloneWidthFifth = editorSidebar.offsetWidth;

  // PLACE ELEMENT
  robotCloneFifth.appendChild(cloneFifth);
  cloneFifth.style.width = cloneWidthFifth + "px";
  cloneFifth.style.left = editorSidebarFooterPosition.left + "px";

  // ADD CLASS HIGHLIGHT
  cloneFifth.querySelector('.editor__sidebar-footer').classList.add('highlight');
// FIrst Robot TAB END]

// Sixth Robot TAB
  // GET ELEMENT
  let editorTextWrapper = document.querySelector('.editor__text-wrapper');
  let robotCloneSixth = document.querySelector('.robot-overlay__clone.sixth');
  let editorTextWrapperPosition = editorTextWrapper.getBoundingClientRect();

  // CLONE ELEMENT
  let cloneSixth = editorTextWrapper.cloneNode(true);
  let cloneWidthSixth = editorTextWrapper.offsetWidth;

  // PLACE ELEMENT
  robotCloneSixth.appendChild(cloneSixth);
  cloneSixth.style.width = cloneWidthSixth + "px";
  cloneSixth.style.position = "absolute";
  cloneSixth.style.left = editorTextWrapperPosition.left + "px";
  cloneSixth.style.top = 47 + "px";

  // ADD CLASS HIGHLIGHT
  cloneSixth.classList.add('highlight');
// Sixth Robot TAB END]

// Eighth Robot TAB
  // GET ELEMENT
  let editorHead = document.querySelector('.editor__head');
  let robotCloneEighth = document.querySelector('.robot-overlay__clone.eighth');
  let editorHeadPosition = editorHead.getBoundingClientRect();

  // CLONE ELEMENT
  let cloneEighth = editorHead.cloneNode(true);
  let cloneWidthEighth = editorHead.offsetWidth;

  // PLACE ELEMENT
  robotCloneEighth.appendChild(cloneEighth);
  cloneEighth.style.width = cloneWidthEighth + "px";
  cloneEighth.style.position = "absolute";
  cloneEighth.style.left = editorHeadPosition.left + "px";

  // ADD CLASS HIGHLIGHT
  cloneEighth.classList.add('highlight');
// Eighth Robot TAB END]

// Ninth Robot TAB
  // GET ELEMENT
  let editorFooter = document.querySelector('.editor__footer');
  let robotCloneNinth = document.querySelector('.robot-overlay__clone.ninth');
  let editorFooterPosition = editorFooter.getBoundingClientRect();

  // CLONE ELEMENT
  let cloneNinth = editorFooter.cloneNode(true);
  let cloneWidthNinth = editorFooter.offsetWidth;

  // PLACE ELEMENT
  robotCloneNinth.appendChild(cloneNinth);
  cloneNinth.style.width = cloneWidthNinth + "px";
  cloneNinth.style.position = "absolute";
  cloneNinth.style.left = editorFooterPosition.left + "px";

  // ADD CLASS HIGHLIGHT
  // cloneNinth.classList.add('highlight');
  cloneNinth.querySelector('.editor__footer-check').classList.add('highlight');
// Ninth Robot TAB END]

// Tenth Robot TAB
  // GET ELEMENT
  let robotCloneTenth = document.querySelector('.robot-overlay__clone.tenth');

  // CLONE ELEMENT
  let cloneTenth = editorFooter.cloneNode(true);
  let cloneWidthTenth = editorFooter.offsetWidth;

  // PLACE ELEMENT
  robotCloneTenth.appendChild(cloneTenth);
  cloneTenth.style.width = cloneWidthTenth + "px";
  cloneTenth.style.position = "absolute";
  cloneTenth.style.left = editorFooterPosition.left + "px";

  // ADD CLASS HIGHLIGHT
  cloneTenth.querySelector('.editor__footer-check').classList.add('highlight');
// Tenth Robot TAB END]

// Eleventh Robot TAB
  // GET ELEMENT
  let robotCloneEleventh = document.querySelector('.robot-overlay__clone.eleventh');

  // CLONE ELEMENT
  let cloneEleventh = editorFooter.cloneNode(true);
  let cloneWidthEleventh = editorFooter.offsetWidth;

  // PLACE ELEMENT
  robotCloneEleventh.appendChild(cloneEleventh);
  cloneEleventh.style.width = cloneWidthEleventh + "px";
  cloneEleventh.style.position = "absolute";
  cloneEleventh.style.left = editorFooterPosition.left + "px";

  // ADD CLASS HIGHLIGHT
  cloneEleventh.querySelector('.editor__footer-check').classList.add('highlight');
// Eleventh Robot TAB END]


// Ace Editor
var editor = ace.edit("editor");
editor.setTheme("ace/theme/tomorrow_night_bright");
editor.session.setMode("ace/mode/javascript");
editor.setOptions({
  fontSize: "18px"
});