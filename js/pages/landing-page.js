/* SWIPER */
var swiper = new Swiper(".mySwiper", {
  centeredSlides: true, 
  slidesPerView: 1,
  spaceBetween: 10,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  breakpoints: {
    576: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    1200: {
      slidesPerView: 2,
      spaceBetween: 30,
    },
    1800: {
      slidesPerView: 3,
      spaceBetween: 40,
    }
  },
});

/* CHANGE IMAGE BASED ON ACCORDION */

// GET BUTTON ACCORDION
const featureFirst = document.getElementById('feature-first');
const featureSecond = document.getElementById('feature-second');
const featureThird = document.getElementById('feature-third');
const featureFourth = document.getElementById('feature-fourth');
const featureFifth = document.getElementById('feature-fifth');

// GET IMAGE
const featureImage = document.getElementById('feature__image');

// CHANGE IMAGE BASED ON CLICKED
featureFirst.addEventListener('click', function(){
  featureImage.src = 'img/landing-page/feature-slideVideo.svg';
})

featureSecond.addEventListener('click', function(){
  featureImage.src = 'img/landing-page/feature-codeEditor.svg';
})

featureThird.addEventListener('click', function(){
  featureImage.src = 'img/landing-page/feature-reviewProfesional.svg';
})

featureFourth.addEventListener('click', function(){
  featureImage.src = 'img/landing-page/feature-quiz.svg';
})

featureFifth.addEventListener('click', function(){
  featureImage.src = 'img/landing-page/feature-onlineMentoring.svg';
})