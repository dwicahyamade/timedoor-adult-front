<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/account.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Account Info - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "component/header.php" ?>

  <main>
    <section class="account">
      <div class="container">
        <h1 class="section__title">
          <a href="dashboard.php" class="btn-back"><i class="fas fa-angle-left"></i></a>
          Account Settings
        </h1>
        <div class="row">
          <div class="col-lg-4 account-navigation">
            <div class="section__bg account-setting__bg h-100">
              <div class="account-navigation__wrapper">
                <a href="account-info.php" class="account-navigation__item active">
                  <i class="far fa-user mr-3"></i> User Info</a>
                <a href="account-security.php" class="account-navigation__item">
                  <img src="img/icon/icon-lock.svg" alt="Security" class="mr-3"> Security</a>
                <a href="account-delete.php" class="account-navigation__item">
                  <img src="img/icon/icon-delete-account.svg" alt="Delete Account" class="mr-2"> Delete Account</a>
                <a href="index.php" class="account-navigation__item--green ">
                  <i class="fas fa-sign-out-alt mr-2"></i> Log out</a>
              </div>
            </div>
          </div>
          <div class="col-lg-8 mt-3 mt-lg-0 pl-lg-0 account-info">
            <div class="section__bg account-setting__bg">
              <div class="alert alert-custom alert-custom__success" role="alert">
                Your profile has been updated!
              </div>
              <form class="form">
                <div class="row">
                  <div class="col-2">
                    <label for="file-input">
                      <div class="account-info__image-wrapper">
                        <img src="img/user-default-white.png" alt="User" class="img-thumbnail account-info__image">
                      </div>
                    </label>
                    <input id="file-input" type="file" class="d-none">
                    <p class="account-info__image-text">Allowed JPG, GIF or PNG. Max size of 800kB</p>
                  </div>
                  <div class="col-10">
                    <div class="form-group position-relative">
                      <label for="emailusername" class="form__title px-1">
                        <i class="far fa-user mr-2"></i> Full Name</label>
                      <input type="email" class="form-control form__input" id="emailusername">
                    </div>
                    <div class="row">
                      <div class="col-6">
                        <div class="form-group position-relative">
                          <label for="phone" class="form__title px-1">
                            <img src="img/icon/icon-tel.svg" alt="Phone" class="mr-2"> Phone number</label>
                          <input type="tel" class="form-control form__input" id="phone">
                        </div>
                        <div class="form-group position-relative">
                          <label for="gender" class="form__title px-1">
                            <i class="far fa-user mr-2"></i> Gender</label>
                          <select name="" id="gender" class="form-control form__input selectpicker py-0">
                            <option value="">Male</option>
                            <option value="">Female</option>
                          </select>
                        </div>
                        <div class="form-group position-relative">
                          <label for="province" class="form__title px-1">
                            <img src="img/icon/icon-location.svg" alt="province" class="mr-2"> Province
                          </label>
                          <input type="text" class="form-control form__input" id="province">
                        </div>
                      </div>
                      <div class="col-6">
                        <div class="form-group position-relative">
                          <label for="location" class="form__title px-1">
                            <i class="far fa-envelope mr-2"></i> Email</label>
                          </label>
                          <input type="email" class="form-control is-invalid form__input" id="location">
                          <div class="invalid-feedback form__custom-invalid">
                            Invalid email address
                          </div>
                        </div>
                        <div class="form-group position-relative">
                          <label for="birthday" class="form__title px-1">
                            <img src="img/icon/icon-birthday.svg" alt="Birthday" class="mr-2"> Birthday</label>
                          <div class="row">
                            <div class="col pr-2"><input type="number" class="form-control form__input--small" id="day" placeholder="Day" max="31"></div>
                            <span class="form__slash">/</span>
                            <div class="col-5">
                              <select class="form-control form__input--small selectpicker" id="month">
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                              </select>
                            </div>
                            <span class="form__slash">/</span>
                            <div class="col pr-2"><input type="number" class="form-control form__input--small" id="year" placeholder="Year"></div>
                          </div>
                        </div>
                        <div class="form-group position-relative">
                          <label for="city" class="form__title px-1">
                            <img src="img/icon/icon-city.svg" alt="city" class="mr-2"> City
                          </label>
                          <input type="text" class="form-control form__input" id="city">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-10 ml-auto">
                    <button type="button" class="btn btn-dark w-100 account-info__btn btn-save">Update Profile</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <?php require_once "component/footer.php" ?>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- selectpicker -->
  <script src="js/bootstrap-select.js"></script>

  <!-- Form -->
  <script src="js/form.js"></script>

  <!-- Custom -->
  <script src="js/pages/account.js"></script>
</body>

</html>