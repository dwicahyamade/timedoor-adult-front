<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/sign-up.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Sign Up - Timedoor Coding Academy</title>
</head>

<body class="sign-up-bg">
  <header>
    <div class="header py-3">
      <div class="container">
        <nav class="navbar navbar-dark bg-transparent">
          <a class="navbar-brand" href="#">
            <img src="img/timedoor-logo-black.png" width="168" height="39" alt="Timedoor Logo">
          </a>
          <div class="my-2 my-lg-0">
            <a href="index.php" class="btn btn-outline-dark my-2 my-sm-0" type="submit">Login</a>
            <div class="btn-group btn-language">
              <button type="button" class="btn btn-transparent" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
                English <i class="fas fa-chevron-down fa-sm arrow-down"></i></button>
              </button>
              <div class="dropdown-menu dropdown-menu-right dropdown-custom-nav">
                <a class="dropdown-item dropdown-custom-nav__item" href="">English</a>
                <a class="dropdown-item dropdown-custom-nav__item" href="">Indonesia</a>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </header>
  <main>
    <section class="banner">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-6">
            <h1 class="banner__title">Get Started and join us</h1>
            <form class="form" action="sign-up-success.php">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group position-relative mb-3">
                    <label for="fullName" class="form__title px-1">
                      <i class="far fa-user mr-2"></i> Full Name</label>
                    <input type="text" class="form-control form__input" id="fullName">
                  </div>
                  <div class="form-group position-relative mb-3">
                    <label for="phone" class="form__title px-1">
                      <img src="img/icon/icon-tel.svg" alt="Phone" class="mr-2"> Phone number</label>
                    <input type="tel" class="form-control form__input" id="phone">
                  </div>
                  <div class="form-group position-relative mb-3">
                    <label for="email" class="form__title px-1">
                      <i class="far fa-envelope mr-2"></i> Email</label>
                    </label>
                    <input type="email" class="form-control is-invalid form__input" id="email">
                    <div class="invalid-feedback form__custom-invalid">
                      Invalid email address
                    </div>
                  </div>
                  <div class="form-group position-relative mb-3">
                    <label for="password" class="form__title px-1">
                      <img src="img/icon/icon-lock.svg" alt="Password" class="mr-2"> Password</label>
                    </label>
                    <div class="input-password">
                      <input type="password" class="form-control form__input--password" id="password">
                      <i class="far fa-eye input-password__icon"></i>
                    </div>
                  </div>
                </div>
                <div class="col-12">
                  <button type="submit" class="btn btn-dark btn-block">Sign Up Now</button>
                </div>
              </div>
            </form>
            <p class="form__member text-center">By signing up for Techdemia, you agree to Techdemia's
              <a href="privacy-terms.php" class="form__member--green d-block">Terms of Service & Privacy Policy.</a></p>
          </div>
        </div>
      </div>
    </section>
  </main>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Select picker -->
  <script src="js/bootstrap-select.js"></script>

  <!-- Form -->
  <script src="js/form.js"></script>

</body>

</html>