<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/privacy-terms.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Terms & Conditions - Timedoor Coding Academy</title>
</head>

<body>
  
  <header class="nav-custom">
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container container-custom">
				<a class="navbar-brand" href="dashboard.php"><img src="img/timedoor-logo-black.png" alt="Timedoor Logo" width="141" height="33"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
				<div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-2 my-1">
              <a href="sign-up.php" class="btn btn-dark btn-start-free-trial mr-2 w-100" type="submit">Sign Up</a>
            </li>
            <li class="nav-item mx-2 my-1">
              <a href="index.php" class="btn btn-outline-dark h-100 w-100" type="submit">Log In</a>
            </li>
            <li class="nav-item">
              <div class="btn-group btn-language w-100">
                <button type="button" class="btn btn-transparent" data-toggle="dropdown" aria-haspopup="true"
                  aria-expanded="false">
                  English
                  <i class="fas fa-chevron-down fa-sm arrow-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-custom-nav position-absolute">
                  <a class="dropdown-item dropdown-custom-nav__item" href="">English</a>
                  <a class="dropdown-item dropdown-custom-nav__item" href="">Indonesia</a>
                </div>
              </div>
            </li>
          </ul>
				</div>
			</div>
		</nav>
	</header>

  <main>

	<section class="policy">
    <div class="container">
      <h1 class="policy__title">Privacy Policy & Terms of Service</h1>
      <div class="policy__inner">
        <div class="accordion" id="accordionPolicy">

          <div class="policy__card">
            <div class="policy__card-header" id="headingOne">
              <h3 class="mb-0">
                <button class="btn policy__accordion-title" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Privacy Policy
                  <span class="policy__accordion-icon"></span>
                </button>
              </h3>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionPolicy">
              <div class="policy__accordion-body">
                <h4 class="policy__accordion-subtitle">Kebijakan Privasi ini terakhir diperbarui pada April 1, 2021.</h4>
                <p class="policy__accordion-text">
                  Terima kasih telah bergabung dengan marketplace pembelajaran online terbesar di dunia. Kami di Udemy, Inc. (“Udemy”, “kami”) menghormati privasi Anda dan ingin agar Anda memahami cara kami mengumpulkan, menggunakan, dan membagikan data tentang Anda. Kebijakan Privasi ini mencakup praktik pengumpulan data kami dan menjelaskan hak Anda untuk mengakses, memperbaiki, atau membatasi penggunaan kami terhadap data pribadi Anda.
                </p>
                <p class="policy__accordion-text">
                  Kecuali jika kami menautkan ke kebijakan lain atau menyatakan lain, Kebijakan Privasi ini berlaku saat Anda mengunjungi atau menggunakan situs web, aplikasi seluler, API, atau layanan terkait Udemy (“Layanan”). Ini juga berlaku untuk calon pelanggan produk bisnis dan perusahaan kami. 
                </p>
                <p class="policy__accordion-text">
                  Dengan menggunakan Layanan, Anda menyetujui ketentuan Kebijakan Privasi ini. Anda sebaiknya tidak menggunakan Layanan jika Anda tidak menyetujui Kebijakan Privasi ini atau perjanjian lainnya yang mengatur penggunaan Layanan oleh Anda. 
                </p>
                <p class="policy__accordion-text">
                  Kami mengumpulkan data tertentu dari Anda secara langsung, seperti informasi yang Anda masukkan sendiri, data tentang penggunaan konten Anda, dan data dari platform pihak ketiga yang Anda hubungkan ke Udemy. Kami juga mengumpulkan sebagian data secara otomatis, seperti informasi tentang perangkat Anda dan dengan bagian apa dari Layanan kami Anda berinteraksi atau menghabiskan waktu saat menggunakannya.
                </p>
              </div>
            </div>
          </div>

          <hr>

          <div class="policy__card">
            <div class="policy__card-header" id="headingTwo">
              <h3 class="mb-0">
                <button class="btn policy__accordion-title collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Terms of Service
                  <span class="policy__accordion-icon"></span>
                </button>
              </h3>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionPolicy">
              <div class="policy__accordion-body">
              <h4 class="policy__accordion-subtitle">Kebijakan Privasi ini terakhir diperbarui pada April 1, 2021.</h4>
                <p class="policy__accordion-text">
                  Terima kasih telah bergabung dengan marketplace pembelajaran online terbesar di dunia. Kami di Udemy, Inc. (“Udemy”, “kami”) menghormati privasi Anda dan ingin agar Anda memahami cara kami mengumpulkan, menggunakan, dan membagikan data tentang Anda. Kebijakan Privasi ini mencakup praktik pengumpulan data kami dan menjelaskan hak Anda untuk mengakses, memperbaiki, atau membatasi penggunaan kami terhadap data pribadi Anda.
                </p>
                <p class="policy__accordion-text">
                  Kecuali jika kami menautkan ke kebijakan lain atau menyatakan lain, Kebijakan Privasi ini berlaku saat Anda mengunjungi atau menggunakan situs web, aplikasi seluler, API, atau layanan terkait Udemy (“Layanan”). Ini juga berlaku untuk calon pelanggan produk bisnis dan perusahaan kami. 
                </p>
                <p class="policy__accordion-text">
                  Dengan menggunakan Layanan, Anda menyetujui ketentuan Kebijakan Privasi ini. Anda sebaiknya tidak menggunakan Layanan jika Anda tidak menyetujui Kebijakan Privasi ini atau perjanjian lainnya yang mengatur penggunaan Layanan oleh Anda. 
                </p>
                <p class="policy__accordion-text">
                  Kami mengumpulkan data tertentu dari Anda secara langsung, seperti informasi yang Anda masukkan sendiri, data tentang penggunaan konten Anda, dan data dari platform pihak ketiga yang Anda hubungkan ke Udemy. Kami juga mengumpulkan sebagian data secara otomatis, seperti informasi tentang perangkat Anda dan dengan bagian apa dari Layanan kami Anda berinteraksi atau menghabiskan waktu saat menggunakannya.
                </p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>
    
  </main>

  <footer class="footer">
    <div class="container">
      <p class="footer__text">Timedoor Coding Academy Adult ©2021 PT. Timedoor Indonesia All Right Reserved</p>
    </div>
  </footer>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- MAKE ACCORDION ALWAYS OPEN ONE -->
  <script>
    jQuery(function ($) {
      $('.policy__accordion-title').on('click', function (e) {
        e.preventDefault();
        if (!$(this).hasClass('collapsed')) {
          e.stopPropagation();
          return false;
        }
      });
    });
  </script>

</body>

</html>