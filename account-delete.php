<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/account.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Account Info - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "component/header.php" ?>

  <main>
    <section class="account">
      <div class="container">
        <h1 class="section__title">
          <a href="dashboard.php" class="btn-back"><i class="fas fa-angle-left"></i></a>
          Account Settings
        </h1>
        <div class="row">
          <div class="col-lg-4 account-navigation">
            <div class="section__bg account-setting__bg h-100">
              <div class="account-navigation__wrapper">
                <a href="account-info.php" class="account-navigation__item">
                  <i class="far fa-user mr-3"></i> User Info</a>
                <a href="account-security.php" class="account-navigation__item">
                  <img src="img/icon/icon-lock.svg" alt="Security" class="mr-3"> Security</a>
                <a href="account-delete.php" class="account-navigation__item active">
                  <img src="img/icon/icon-delete-account.svg" alt="Delete Account" class="mr-2"> Delete Account</a>
                <a href="index.php" class="account-navigation__item--green ">
                  <i class="fas fa-sign-out-alt mr-2"></i> Log out</a>
              </div>
            </div>
          </div>
          <div class="col-lg-8 mt-3 mt-lg-0 pl-lg-0 account-info">
            <div class="section__bg account-setting__bg">
              <h1 class="section__title account-info__title">Delete This Account</h1>
              <p class="account-info__desc">You can delete your account at any time; however, this action is
                irreversible. Once this account deleted, every data, submision and material can't be restored.</p>
              <form class="form">
                <div class="row">
                  <div class="col-lg-7">
                    <div class="form-group position-relative mb-3">
                      <label for="location" class="form__title px-1">
                        <i class="far fa-envelope mr-2"></i> Email Adress</label>
                      </label>
                      <input type="email" class="form-control form__input" id="location">
                      <div class="invalid-feedback">
                        Invalid email address
                      </div>
                    </div>
                    <div class="form-group position-relative mb-3">
                      <label for="password" class="form__title px-1">
                        <img src="img/icon/icon-lock.svg" alt="Password" class="mr-2"> Password</label>
                      </label>
                      <input type="password" class="form-control form__input" id="password">
                    </div>
                    <button type="submit" class="btn btn-red w-100 account-info__btn">Delete Account</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <?php require_once "component/footer.php" ?>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>