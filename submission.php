<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/submision.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Submision List - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "component/header.php" ?>

  <main>
    <section class="submission">
      <div class="container">
        <div class="submission__head">
          <h1 class="section__title submission__title">Setyo's Submission List</h1>
          <div class="submission__sort">
            <img src="img/icon/icon-sort.svg" alt="Sort"> Sort By
            <div class="form-group btn-sort ml-2 border">
              <select name="" id="" class="btn-sort__wrap py-0 selectpicker">
                <option value="Oldest">Latest</option>
                <option value="Oldest">Oldest</option>
                <option value="A → Z">A → Z</option>
                <option value="Z → A">Z → A</option>
              </select>
            </div>
          </div>
          <div class="submission__search">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <button class="btn bg-white border border-right-0" type="button" id="button-addon1"><i
                    class="fas fa-search"></i>
                </button>
              </div>
              <input type="text" class="form-control border" placeholder="Search" aria-label="Search"
                aria-describedby="button-addon1">
            </div>
          </div>
        </div>
        <table class="submision__table custom-table">
          <thead>
            <tr>
              <th class="custom-table__head-td" width="250px">Chapter and Topic</th>
              <th class="custom-table__head-td" width="250px">Project Title</th>
              <th class="custom-table__head-td" width="170px">Last Update</th>
              <th class="custom-table__head-td" width="150px">Status</th>
              <th class="custom-table__head-td" width="150px">Badge</th>
              <th class="custom-table__head-td" width="230px">Action</th>
            </tr>
          </thead>
          <tbody class="custom-table__body">
            <tr>
              <td class="custom-table__body-td"><b>Ch.4</b> - Topic 1 (Eksternal CSS</td>
              <td class="custom-table__body-td">Eksternal CSS Practice</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <a role="button" href="coding-submission-inReview.php" class="d-flex align-items-baseline cursor">
                  <i class="fas fa-circle mr-1"></i>
                  in Review (dev purpose)
                </a>
              <td class="custom-table__body-td--muted">Not Available</td>
              <td class="custom-table__body-td"><a href="#" class="btn btn-dark disabled">View
                  Comment</a>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td"><b>Ch.4</b> - Topic 1 (Eksternal CSS</td>
              <td class="custom-table__body-td">Eksternal CSS Practice</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td text-grey">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1"></i>
                  Waiting Review
                </span>
              </td>
              <td class="custom-table__body-td--muted">Not Available</td>
              <td class="custom-table__body-td"><a href="coding-submission-waitingReview.php" class="btn btn-dark">View
                  Comment</a></td>
            </tr>
            <tr>
              <td class="custom-table__body-td"><b>Ch.4</b> - Topic 1 (Eksternal CSS</td>
              <td class="custom-table__body-td">Eksternal CSS Practice</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-warning"></i>
                  Recode
                </span>
              </td>
              <td class="custom-table__body-td--muted">Not Available</td>
              <td class="custom-table__body-td">
                <a href="coding-submission-reCode.php" class="btn btn-dark">
                  View Comment</a>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td"><b>Ch.4</b> - Topic 1 (Eksternal CSS</td>
              <td class="custom-table__body-td">Final Exam</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td text-green">
                <span class="badge badge-green">Excellent</span>
              </td>
              <td class="custom-table__body-td">
                <a href="coding-submission-finishedReview.php" class="btn btn-dark">View Comment</a>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td"><b>Ch.4</b> - Topic 1 (Eksternal CSS</td>
              <td class="custom-table__body-td">Final Exam</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td text-green">
                <span class="badge badge-darkgreen">Good</span>
              </td>
              <td class="custom-table__body-td"><button class="btn btn-dark">View Comment</button>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td"><b>Ch.4</b> - Topic 1 (Eksternal CSS</td>
              <td class="custom-table__body-td">Final Exam</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td text-green">
                <span class="badge badge-darkgreen">Good</span>
              </td>
              <td class="custom-table__body-td"><button class="btn btn-dark">View Comment</button></td>
            </tr>
            <tr>
              <td class="custom-table__body-td"><b>Ch.4</b> - Topic 1 (Eksternal CSS</td>
              <td class="custom-table__body-td">Final Exam</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td text-green">
                <span class="badge badge-green">Excellent</span>
              </td>
              <td class="custom-table__body-td">
                <a href="coding-submission-finishedReview.php" class="btn btn-dark">View Comment</a>
              </td>
            <tr>
              <td class="custom-table__body-td"><b>Ch.4</b> - Topic 1 (Eksternal CSS</td>
              <td class="custom-table__body-td">Eksternal CSS Practice</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td--muted">Not Available</td>
              <td class="custom-table__body-td"><button class="btn btn-dark">View Comment</button>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td"><b>Ch.4</b> - Topic 1 (Eksternal CSS</td>
              <td class="custom-table__body-td">Eksternal CSS Practice</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td--muted">Not Available</td>
              <td class="custom-table__body-td"><button class="btn btn-dark">View Comment</button>
              </td>
            </tr>
          </tbody>
        </table>

        <div class="pagination-custom">
          <a href="" class="pagination-custom__item"><i class="fas fa-chevron-left"></i></a>
          <a href="" class="pagination-custom__item active">1</a>
          <a href="" class="pagination-custom__item">2</a>
          <a href="" class="pagination-custom__item">3</a>
          <a href="" class="pagination-custom__item">4</a>
          <a href="" class="pagination-custom__item">5</a>
          <a href="" class="pagination-custom__item">...</a>
          <a href="" class="pagination-custom__item">15</a>
          <a href="" class="pagination-custom__item"><i class="fas fa-chevron-right"></i></a>
        </div>
      </div>
    </section>

    <!-- whatsapp & back to top -->
    <?php require_once 'component/button-whatsapp.php' ?>
  
    <?php require_once 'component/button-back-to-top.php' ?>

  </main>

  <!-- Footer -->
  <?php require_once "component/footer.php" ?>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Selectpicker -->
  <script src="js/bootstrap-select.js"></script>
</body>

</html>