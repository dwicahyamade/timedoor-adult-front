<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/quiz.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Quiz - Timedoor Coding Academy</title>
</head>

<body>
  <?php require_once "component/header.php" ?>
  <section class="header-chapter">
    <div class="container container-custom">
      <div class="header-chapter__container">
        <div class="header-chapter__left">
          <a href="" class="btn-back"><i class="fas fa-angle-left fa-lg"></i></a>
          <h1 class="header-chapter__title">Quiz</h1>
          <p class="header-chapter__subtitle">Chapter 2 - Basic HTML and CSS</p>
        </div>
        <div class="header-chapter__right">
          <i class="fas fa-bars fa-2x side-bar__btn-show"></i>
        </div>
      </div>
    </div>
  </section>


  <!-- Side Bar -->
  <?php require_once "component/side-bar.php" ?>

  <main>
    <section class="quiz">
      <div class="container">
        <div class="quiz__header">
          <div class="progress quiz__progress">
            <div class="progress-bar quiz__progress-bar" role="progressbar" style="width: 10%" aria-valuenow="10"
              aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          <span class="quiz__progress-text">1 / 10 completed</span>
        </div>

        <div class="quiz__content">
          <h2 class="quiz__title">
            The abbreviation HTML stands for which of the following?
          </h2>
          <button type="button" class="quiz__select">
              Cascading Style Sheets
            </button>
          <button type="button" class="quiz__select">
            Cascading Subject Sheets
          </button>
          <button type="button" class="quiz__select">
            Cascading Super Sheets
          </button>
          <button type="button" class="quiz__select">
            Cascading Supra Sheets
          </button>
        </div>
      </div>
    </section>
  </main>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- This Sidebar JS -->
  <script src="js/side-bar.js"></script>

  <!-- This Page JS -->
  <script src="js/pages/quiz.js"></script>

</body>

</html>