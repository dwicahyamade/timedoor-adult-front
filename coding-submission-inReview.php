<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@300&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/coding-submission.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Coding Submission - Timedoor Coding Academy</title>
</head>

<body>
  <?php require_once "component/header.php" ?>
  <section class="header-chapter">
    <div class="container container-custom">
      <div class="header-chapter__container">
        <div class="header-chapter__left">
          <a href="" class="btn-back"><i class="fas fa-angle-left fa-lg"></i></a>
          <h1 class="header-chapter__title">Coding Submission</h1>
        </div>
        <div class="header-chapter__right">
          <div class="header-chapter__right-wrapper">
            <p class="header-chapter__subtitle d-block">Chapter 2 : Meeting 1 ( Show Content : Text )</p>
            <p class="header-chapter__title d-block text-right">Brown Bear Article</p>
          </div>
          <i class="fas fa-bars fa-2x side-bar__btn-show ml-5"></i>
        </div>
      </div>
    </div>
  </section>


  <!-- Side Bar -->
  <?php require_once "component/side-bar.php" ?>

  <main>
    <section class="coding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-8 px-0">
            <div class="coding__left editor">
              <?php require_once "component/sidebar-editor.php" ?>

              <!-- Editor Content -->
              <div class="editor__content">
                <div class="editor__head">
                  <ul class="editor__head-items">
                    <li class="editor__head-item active">HTML</li>
                    <li class="editor__head-item">CSS</li>
                    <li class="editor__head-item">Javascript</li>
                  </ul>
                </div>
                <!-- Overlay -->
                <div class="editor__text-wrapper">
                  <div id="editor">function foo(items) {
                    var x = "All this is syntax highlighted";
                    return x;
                    }
                  </div>
                  <div class="overlay">
                    <div class="overlay__icon-check">
                      <img src="img/icon/icon-submit.svg" alt="Checking Code">
                      <span class="d-block mt-2">Submiting Code...</span>
                    </div>
                    <div class="overlay__submited">
                      <img src="img/icon/icon-code-success.svg" alt="Code Submitted">
                      <span class="overlay__title">Code Submited!</span>
                      <span class="overlay__subtitle">successfully submited to Mentor</span>
                    </div>
                    <div class="overlay__draft">
                      <img src="img/icon/icon-code-success.svg" alt="Code Submitted">
                      <span class="overlay__title">Saved as draft</span>
                      <span class="overlay__subtitle">Your work has been saved</span>
                    </div>
                  </div>
                </div>
                <div class="editor__footer">
                  <span class="editor__footer-text">
                    Preview
                  </span>
                  <span class="editor__footer-text editor__btn-draft disabled mr-0">
                    Save as draft
                  </span>
                  <span class="editor__footer-text--green ml-3 mr-auto">
                    <i class="fas fa-circle mr-1"></i> Mentor is checking your code now
                  </span>
                  <button class="btn btn-green disabled">Update Code</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-4 px-0">
            <div class="coding__right">
              <!-- Accordion -->
              <div class="accordion" id="accordionExample">

                <div class="card border-0">
                  <div class="card-header coding__right-title p-0" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn-requirement" type="button" data-toggle="collapse"
                        data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Project Requirement
                      </button>
                    </h2>
                  </div>

                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                    data-parent="#accordionExample">
                    <div class="card-body coding__right-container">
                      <div class="coding__right-head">
                        <h3 class="coding__right-subtitle">
                          Brown Bear Text Article</h3>
                      </div>
                      <p class="coding__right-desc">
                        Write an article about Brown Bear. The title is The Brown Bear. In this article, there are
                        several
                        sections as follows:
                        <br>1. About Brown Bear <br>2. Species <br>3. Features <br>4. Habitat <br>5. Media <br><br>Each
                        section
                        uses
                        sub headings, and if there are subsections in it you can use the next sub heading. The species
                        section
                        uses an unordered list while the habitat uses an ordered list with numeric types. Add photos and
                        videos about Brown Bears to the media section. Don't forget to use bold, italic, and underline
                        text if
                        needed.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="card border-0">
                  <div class="card-header coding__right-title p-0" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn-requirement collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Mentor's Comment
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body coding__right-container">
                      <div class="d-flex justify-content-between mb-3">
                        <div class="coding__right-name">
                          Bill Gates
                        </div>
                        <div class="coding__right-date">
                          10-10-2021 20:20
                        </div>
                      </div>
                      <p class="coding__right-desc">
                        Your code is great, but you need to pay attention to the identation to make your code tidier.
                        Fix the indentation of the title under the
                        &lt;body&gt; tag, the &lt;p&gt; tag inside the features section, and the &lt;li&gt; inside the
                        &lt;ul&gt; tag in spesies
                        section.
                      </p>
                      <div class="d-flex justify-content-between mb-3">
                        <div class="coding__right-name">
                          Bill Gates
                        </div>
                        <div class="coding__right-date">
                          10-10-2021 20:20
                        </div>
                      </div>
                      <p class="coding__right-desc">
                        Your code is great, but you need to pay attention to the identation to make your code tidier.
                        Fix the indentation of the title under the
                        &lt;body&gt; tag, the &lt;p&gt; tag inside the features section, and the &lt;li&gt; inside the
                        &lt;ul&gt; tag in spesies
                        section.
                      </p>
                      <div class="d-flex justify-content-between mb-3">
                        <div class="coding__right-name">
                          Bill Gates
                        </div>
                        <div class="coding__right-date">
                          10-10-2021 20:20
                        </div>
                      </div>
                      <p class="coding__right-desc">
                        Your code is great, but you need to pay attention to the identation to make your code tidier.
                        Fix the indentation of the title under the
                        &lt;body&gt; tag, the &lt;p&gt; tag inside the features section, and the &lt;li&gt; inside the
                        &lt;ul&gt; tag in spesies
                        section.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Accordion -->
            </div>
          </div>
        </div>
      </div>
    </section>
    
     <!-- Button Whatsapp -->
     <?php require_once 'component/button-whatsapp.php' ?>
     
  </main>

  <!-- Modal -->
  <div class="modal fade modal-custom" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content modal-custom">
        <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-times custom-close__button"></i></span>
        </button>
        <div class="modal-body text-center">
          <img src="img/modal-check-green.svg" alt="Finished">
          <h3 class="modal-custom__title--small">You have finished learning</h3>
          <p class="modal-custom__desc">
            You already finished learning this material, let's continue to the next material
          </p>
          <a href="chapter-slide.php" class="btn btn-dark modal-custom__btn">Continue Next Material</a>
          <a href="dashboard.php" class="modal-custom__footer-text">Back to Material List</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- This Sidebar JS -->
  <script src="js/side-bar.js"></script>

  <!-- Ace Editor -->
  <script src="js/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>

  <!-- This Page JS -->
  <script src="js/pages/coding-submission.js"></script>

</body>

</html>