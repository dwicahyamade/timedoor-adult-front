<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/chapter-slide.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Learning Video - Timedoor Coding Academy</title>
</head>

<body>
  <?php require_once "component/header.php" ?>
  <section class="header-chapter">
    <div class="container container-custom">
      <div class="header-chapter__container">
        <div class="header-chapter__left">
          <a href="dashboard.php" class="btn-back"><i class="fas fa-angle-left fa-lg"></i></a>
          <h1 class="header-chapter__title">Chapter 2</h1>
          <p class="header-chapter__subtitle">Topic 1 - Color, Font Family</p>
        </div>
        <div class="header-chapter__right">
          <i class="fas fa-bars fa-2x side-bar__btn-show"></i>
        </div>
      </div>
    </div>
  </section>


  <!-- Side Bar -->
  <?php require_once "component/side-bar.php" ?>

  <main>
    <section class="slide-first">
      <div class="slide-first__bg">
        <img src="img/icon/Slide_Icon.svg" alt="Slide" class="slide-first__icon">
        <h1 class="slide-first__title">COLOR, FONT FAMILY</h1>
        <p class="slide-first__desc">This topic will contain material about introduction to font color and family</p>
        <p class="slide-first__time">10 Minutes</p>
        <p class="slide-first__scroll">Scroll to read more</p>
        <button class="btn btn-slide scroll" id="btn-scroll"><i class="fas fa-chevron-down"></i></button>
      </div>
    </section>
    <section class="slide-second">
      <div class="position-relative">
        <button class="btn btn-slide up" id="btn-up"><i class="fas fa-chevron-up fa-lg"></i></button>
        <button class="btn btn-slide down" id="btn-down"><i class="fas fa-chevron-down fa-lg"></i></button>
        <div class="slide-second__bg slide-second__image-container">
          <img src="img/video-example.jpg" alt="Slide" class="slide-second__content slide-second__image active">
          <img src="img/video-example-2.jpg" alt="Slide" class="slide-second__content slide-second__image next">
          <img src="img/video-example-3.jpg" alt="Slide" class="slide-second__content slide-second__image next">
          <div class="slide-second__content slide-second__image next">
            <div class="slide-finish">
              <h3 class="slide-finish__title">You already finished reading the material</h3>
              <h4 class="slide-finish__desc">Let's Continue to the Quiz!</h4>
              <a href="quiz.php" class="btn btn-dark">Take The Quiz</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- This Sidebar JS -->
  <script src="js/side-bar.js"></script>

  <!-- This Page JS -->
  <script src="js/pages/chapter-slide.js"></script>
</body>

</html>