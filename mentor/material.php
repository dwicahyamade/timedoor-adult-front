<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="../css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="../css/pages/material.css">


  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="../img/faveicon/timedoor-faveicon.jpg">

  <title>Mentor Material - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "../component/mentor/header.php" ?>
  <main>
    <div class="container">
      <div class="jumbotron jumbotron-fluid jumbotron-custom">
        <div class="container d-flex">
          <div class="jumbotron-custom__text">
            <h1 class="jumbotron-custom__title">Web Development Course</h1>
            <p class="jumbotron-custom__desc">Build a Website With HTML & CSS</p>
          </div>
          <div class="jumbotron-custom__image">
            <img src="../img/mentor-material-jumbotron.png" alt="Timedoor">
          </div>
        </div>
      </div>

      <section class="material">
        <div class="row">
          <div class="col-12">
            <div class="section__bg">
              <div class="accordion" id="Subscribe-Course">
                <!-- Chapter 1 -->
                <div class="border-0">
                  <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                    <button class="btn btn-accordion btn-block" type="button" data-toggle="collapse"
                      data-target="#chapterOne" aria-expanded="true" aria-controls="chapterOne">
                      <h3 class="section__title">Chapter 1 - Basic Concept</h3>
                    </button>
                  </div>
                  <div id="chapterOne" class="collapse show" aria-labelledby="headingOne"
                    data-parent="#Subscribe-Course">
                    <div class="accordion" id="chapterOne-topic">
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterOne-topicOne" aria-expanded="true"
                          aria-controls="chapterOne-topicOne">
                          <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterOne-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterOne-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <a href="coding-submission.php" class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</a>
                            <p class="topic__progress-text">Coding Submission</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterOne-topicTwo" aria-expanded="true"
                          aria-controls="chapterOne-topicTwo">
                          <h4 class="section__title--top topic__title">Topic 2 - Coding Practice</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterOne-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterOne-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterOne-topicThree" aria-expanded="true"
                          aria-controls="chapterOne-topicThree">
                          <h4 class="section__title--top topic__title">Topic 3 - IT Business Literacy</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterOne-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterOne-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
                <!-- Chapter 2 -->
                <div class="border-0">
                  <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                    <button class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
                      data-target="#chapterTwo" aria-expanded="true" aria-controls="chapterTwo">
                      <h3 class="section__title">Chapter 2 - Basic HTML and CSS</h3>
                    </button>
                  </div>
                  <div id="chapterTwo" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
                    <div class="accordion" id="chapterTwo-topic">
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterTwo-topicOne" aria-expanded="true"
                          aria-controls="chapterTwo-topicOne">
                          <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterTwo-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterTwo-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterTwo-topicTwo" aria-expanded="true"
                          aria-controls="chapterTwo-topicTwo">
                          <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterTwo-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterTwo-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterTwo-topicThree" aria-expanded="true"
                          aria-controls="chapterTwo-topicThree">
                          <h4 class="section__title--top topic__title">Topic 3 - Division</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterTwo-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterTwo-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
                <!-- Chapter 3 -->
                <div class="border-0">
                  <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                    <button class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
                      data-target="#chapterThree" aria-expanded="true" aria-controls="chapterThree">
                      <h3 class="section__title">Chapter 3 - HTML & CSS Level Up</h3>
                    </button>
                  </div>
                  <div id="chapterThree" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
                    <div class="accordion" id="chapterThree-topic">
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterThree-topicOne" aria-expanded="true"
                          aria-controls="chapterThree-topicOne">
                          <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterThree-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterThree-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterThree-topicTwo" aria-expanded="true"
                          aria-controls="chapterThree-topicTwo">
                          <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterThree-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterThree-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterThree-topicThree" aria-expanded="true"
                          aria-controls="chapterThree-topicThree">
                          <h4 class="section__title--top topic__title">Topic 3 - Division</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterThree-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterThree-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
                <!-- Chapter 4 -->
                <div class="border-0">
                  <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                    <button class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
                      data-target="#chapterFour" aria-expanded="true" aria-controls="chapterFour">
                      <h3 class="section__title">Chapter 4 - Javascript</h3>
                    </button>
                  </div>
                  <div id="chapterFour" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
                    <div class="accordion" id="chapterFour-topic">
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterFour-topicOne" aria-expanded="true"
                          aria-controls="chapterFour-topicOne">
                          <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterFour-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterFour-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterFour-topicTwo" aria-expanded="true"
                          aria-controls="chapterFour-topicTwo">
                          <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterFour-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterFour-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterFour-topicThree" aria-expanded="true"
                          aria-controls="chapterFour-topicThree">
                          <h4 class="section__title--top topic__title">Topic 3 - Division</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterFour-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterFour-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
                <!-- Chapter 5 -->
                <div class="border-0">
                  <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                    <button class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
                      data-target="#chapterFive" aria-expanded="true" aria-controls="chapterFive">
                      <h3 class="section__title">Chapter 5 - Vue JS</h3>
                    </button>
                  </div>
                  <div id="chapterFive" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
                    <div class="accordion" id="chapterFive-topic">
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterFive-topicOne" aria-expanded="true"
                          aria-controls="chapterFive-topicOne">
                          <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterFive-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterFive-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterFive-topicTwo" aria-expanded="true"
                          aria-controls="chapterFive-topicTwo">
                          <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterFive-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterFive-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                      <div class="accordion-border">
                        <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                          data-toggle="collapse" data-target="#chapterFive-topicThree" aria-expanded="true"
                          aria-controls="chapterFive-topicThree">
                          <h4 class="section__title--top topic__title">Topic 3 - Division</h4>
                          <div class="topic__progress">
                            <div class="pie-wrapper progress-0 style-2">
                              <span class="label">0<span class="smaller">%</span></span>
                              <div class="pie">
                                <div class="left-side half-circle"></div>
                                <div class="right-side half-circle"></div>
                              </div>
                              <div class="pie-shadow"></div>
                            </div>
                          </div>
                          <p class="topic__desc">This topic will contain material about introduction to the internet.
                            Material includes what is the internet, history, and the opportunities of the internet.
                          </p>
                        </button>
                        <div id="chapterFive-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                          data-parent="#chapterFive-topic">
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Paragraph
                              <span class="topic__progress-text--small">15 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Heading
                              <span class="topic__progress-text--small">7 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Bold, Italic, Underline</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 1</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Color, Font Family
                              <span class="topic__progress-text--small">10 min 12 sec</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 2</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-play mr-2"></i>
                              Video</button>
                            <p class="topic__progress-text">Unordered List
                              <span class="topic__progress-text--small">12 min</span>
                            </p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="far fa-clone mr-2"></i>
                              Slide</button>
                            <p class="topic__progress-text">Ordered List</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                              Practice</button>
                            <p class="topic__progress-text">Coding Practice 3</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-code mr-2"></i>
                              Project</button>
                            <p class="topic__progress-text">Coding Submision</p>
                          </div>
                          <div class="topic-inner__detail my-2">
                            <div class="form-check">
                              <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                                value="option1">
                            </div>
                            <button class="btn btn-dark--small topic-inner__button mx-3"><i
                                class="fas fa-puzzle-piece mr-2"></i>
                              Quiz</button>
                            <p class="topic__progress-text">Showing Text Content</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- back to top -->
      <button class="btn-back-to-top">
        <i class="fas fa-angle-double-up"></i>
      </button>
    </div>
  </main>

  <?php require_once "../component/mentor/footer.php" ?>

  <!-- Bootstrap -->
  <script src="../js/jquery-3.5.1.slim.min.js"></script>
  <script src="../js/bootstrap.bundle.min.js"></script>

  <!-- Layout -->
  <script src="../js/pages/layout.js"></script>

  <!-- This Page Only JS -->
  <script src="../js/pages/dashboard.js"></script>
</body>

</html>