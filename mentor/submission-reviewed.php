<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="../css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="../css/pages/submision.css">


  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="../img/faveicon/timedoor-faveicon.jpg">

  <title>Mentor Submission - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "../component/mentor/header.php" ?>
  <main>
    <div class="container">
      <section class="categories">
        <div class="">
          <h2 class="section__title">Categories</h2>
        </div>
        <div class="categories__wrapper">
          <a href="submission.php" class="card-custom">
            <div class="card-custom__bg darkgreen">
              <img src="../img/icon/Icon-download.svg" alt="New Submission">
            </div>
            <div class="card-custom__text">
              <h2 class="card-custom__title">New Submission</h2>
              <span class="card-custom__title-badge">4</span>
            </div>
          </a>
          <a href="submission-inReview.php" class="card-custom">
            <div class="card-custom__bg orange">
              <img src="../img/icon/icon-setting.png" alt="In Review">
            </div>
            <div class="card-custom__text">
              <h2 class="card-custom__title">In Review</h2>
              <span class="card-custom__title-badge">4</span>
            </div>
          </a>
          <a href="submission-reviewed.php" class="card-custom">
            <div class="card-custom__bg green">
              <img src="../img/icon/Icon-checkmark-circle.svg" alt="Reviewed">
            </div>
            <div class="card-custom__text">
              <h2 class="card-custom__title">Reviewed</h2>
            </div>
          </a>
        </div>
      </section>

      <section class="submission">
        <div class="submission__head">
          <h1 class="section__title submission__title border-green">Reviewed</h1>
          <div class="submission__sort">
            <label for="sort">
              <img src="../img/icon/icon-sort.svg" alt="Sort"> Sort By
            </label>
            <div class="form-group btn-sort ml-2 border">
              <select name="" id="sort" class="btn-sort__wrap py-0 selectpicker">
                <option value="Oldest">Latest</option>
                <option value="Oldest">Oldest</option>
                <option value="A → Z">A → Z</option>
                <option value="Z → A">Z → A</option>
              </select>
            </div>
          </div>
          <div class="submission__search">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <button class="btn bg-white border border-right-0" type="button" id="button-addon1"><i
                    class="fas fa-search"></i>
                </button>
              </div>
              <input type="text" class="form-control border" placeholder="Search" aria-label="Search"
                aria-describedby="button-addon1">
            </div>
          </div>
        </div>
        <table class="submision__table custom-table">
          <thead>
            <tr>
              <th class="custom-table__head-td" width="250px">Student's Name</th>
              <th class="custom-table__head-td" width="320px">Submission Material</th>
              <th class="custom-table__head-td" width="160px">Last Update</th>
              <th class="custom-table__head-td" width="150px">Reviewed</th>
              <th class="custom-table__head-td p-0" width="150px">
                <select name="" id="" title="Review By" class="btn-reviewby selectpicker"
                  data-style="btn-reviewby__wrap" data-width="150px">
                  <option class="btn-reviewby__list" value="Me">Me</option>
                  <option class="btn-reviewby__list" value="Wanda">Wanda</option>
                  <option class="btn-reviewby__list" value="Gita">Gita</option>
                </select>
              </th>
              <th class="custom-table__head-td" width="190px">Status</th>
            </tr>
          </thead>
          <tbody class="custom-table__body">
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.2</b> - Topic 1 (Show Content...</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Me</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Finished
                </span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.2</b> - Topic 1 (Show Content...</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Me</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Finished
                </span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.2</b> - Topic 1 (Show Content...</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Ms. Gita</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-warning"></i>
                  In Revision
                </span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.2</b> - Topic 1 (Show Content...</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Ms. Wanda</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Finished
                </span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.2</b> - Topic 1 (Show Content...</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Ms. Gita</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-warning"></i>
                  In Revision
                </span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.1</b> - Final Exam
                <i class="fas fa-star star-badge darkgreen ml-2"></i></td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Ms. Wanda</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Finished
                </span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.1</b> - Final Exam
                <i class="fas fa-star star-badge darkgreen ml-2"></i></td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Ms. Wanda</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Finished
                </span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.2</b> - Topic 1 (Show Content...</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Ms. Gita</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-warning"></i>
                  In Revision
                </span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.1</b> - Final Exam
                <i class="fas fa-star star-badge green ml-2"></i></td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Ms. Wanda</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Finished
                </span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  Setyo Syahindra
                </span>
              </td>
              <td class="custom-table__body-td"><b>Ch.3</b> - Final Exam
                <i class="fas fa-star star-badge orange ml-2"></i></td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">11-10-2021</td>
              <td class="custom-table__body-td">Ms. Wanda</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Finished
                </span>
              </td>
            </tr>
          </tbody>
        </table>

        <div class="pagination-custom">
          <a href="" class="pagination-custom__item"><i class="fas fa-chevron-left"></i></a>
          <a href="" class="pagination-custom__item active">1</a>
          <a href="" class="pagination-custom__item">2</a>
          <a href="" class="pagination-custom__item">3</a>
          <a href="" class="pagination-custom__item">4</a>
          <a href="" class="pagination-custom__item">5</a>
          <a href="" class="pagination-custom__item"></a>
          <a href="" class="pagination-custom__item">15</a>
          <a href="" class="pagination-custom__item"><i class="fas fa-chevron-right"></i></a>
        </div>
      </section>

      <!-- back to top -->
      <button class="btn-back-to-top">
        <i class="fas fa-angle-double-up"></i>
      </button>
    </div>
  </main>

  <?php require_once "../component/mentor/footer.php" ?>

  <!-- Bootstrap -->
  <script src="../js/jquery-3.5.1.slim.min.js"></script>
  <script src="../js/bootstrap.bundle.min.js"></script>

  <!-- Select picker -->
  <script src="../js/bootstrap-select.js"></script>

  <!-- Layout -->
  <script src="../js/pages/layout.js"></script>

  <!-- This Page Only JS -->

</body>

</html>