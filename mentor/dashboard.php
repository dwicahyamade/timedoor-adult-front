<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="../css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="../css/pages/dashboard.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="../img/faveicon/timedoor-faveicon.jpg">

  <title>Mentor Dashboard - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "../component/mentor/header.php" ?>
  <main>
    <div class="container">
      <div class="jumbotron jumbotron-custom">
        <div class="container">
          <div class="jumbotron-custom__text w-50">
            <h1 class="jumbotron-custom__title">Welcome, Bill Gates</h1>
            <p class="jumbotron-custom__desc">Let's bost your skill with flexible learning. You've been learning
              building a website using HTML & CSS, Keep going on your progress!</p>
          </div>
          <div class="jumbotron-custom__image">
            <img src="../img/dashboard-jumbotron.png" alt="Timedoor">
          </div>
        </div>
      </div>

      <section class="overview">
        <div class="d-flex justify-content-between align-items-center">
          <h2 class="section__title--big">Student's Overview</h2>
          <div class="total-student">
            <div class="total-student__img-container">
              <div class="total-student__img">
                <img src="../img/icon/icon-person.svg" alt="Student">
              </div>
              <span class="total-student__text">Total Students</span>
            </div>
            <span class="total-student__number">30</span>
          </div>
        </div>
      </section>

      <section class="categories">
        <div class="d-flex justify-content-between">
          <h2 class="section__title">Categories</h2>
          <div class="arrow-slider d-flex">
            <i class="fas fa-chevron-circle-left fa-2x arrow-slider__left  mr-2 disabled"></i>
            <i class="fas fa-chevron-circle-right fa-2x arrow-slider__right  ml-2"></i>
          </div>
        </div>
        <div class="categories__wrapper">
          <div class="card-custom">
            <div class="card-custom__bg orange">
              <img src="../img/icon/icon-head.svg" alt="Head">
            </div>
            <div class="card-custom__text">
              <h2 class="card-custom__title">Chapter 1</h2>
              <p class="card-custom__desc">Basic Concept</p>
            </div>
          </div>
          <a href="dashboard-2.php" class="card-custom">
            <div class="card-custom__bg green">
              <img src="../img/icon/Icon-html5.svg" alt="HTML5">
            </div>
            <div class="card-custom__text">
              <h2 class="card-custom__title">Chapter 2</h2>
              <p class="card-custom__desc">Basic HTML and CSS</p>
            </div>
          </a>
          <div class="card-custom">
            <div class="card-custom__bg darkgreen">
              <img src="../img/icon/icon-coding.svg" alt="CODING">
            </div>
            <div class="card-custom__text">
              <h2 class="card-custom__title">Chapter 3</h2>
              <p class="card-custom__desc">HTML & CSS Level Up</p>
            </div>
          </div>
          <div class="card-custom">
            <div class="card-custom__bg darkblue">
              <img src="../img/icon/icon-js.svg" alt="CODING">
            </div>
            <div class="card-custom__text">
              <h2 class="card-custom__title">Chapter 4</h2>
              <p class="card-custom__desc">Javascript</p>
            </div>
          </div>
          <div class="card-custom">
            <div class="card-custom__bg dark">
              <img src="../img/icon/icon-vue_js.svg" alt="CODING">
            </div>
            <div class="card-custom__text">
              <h2 class="card-custom__title">Chapter 5</h2>
              <p class="card-custom__desc">Vue JS</p>
            </div>
          </div>
        </div>
      </section>

      <!-- Submission -->
      <section class="submission">
        <div class="submission__head">
          <h2 class="section__title submission__title border-orange">Student's Progress / Chapter 1</h2>
          <div class="submission__sort">
            <img src="../img/icon/icon-sort.svg" alt="Sort"> Sort By
            <div class="form-group btn-sort ml-2 border">
              <select name="" id="" class="btn-sort__wrap py-0 selectpicker">
                <option value="Oldest">Oldest</option>
                <option value="Oldest">Status</option>
              </select>
            </div>
          </div>
          <div class="submission__search">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <button class="btn bg-white border border-right-0" type="button" id="button-addon1"><i class="fas fa-search"></i>
                </button>
              </div>
              <input type="text" class="form-control border" placeholder="Search" aria-label="Search" aria-describedby="button-addon1">
            </div>
          </div>
        </div>
        <table class="submision__table custom-table">
          <thead>
            <tr>
              <th class="custom-table__head-td" width="250px">Student Name</th>
              <th class="custom-table__head-td" width="170px">Start Date</th>
              <th class="custom-table__head-td" width="170px">Last Learning</th>
              <th class="custom-table__head-td" width="280px">Learning Progress</th>
              <th class="custom-table__head-td" width="130px">Status</th>
              <th class="custom-table__head-td text-center" width="180px">Action</th>
            </tr>
          </thead>
          <tbody class="custom-table__body">
            <tr>
              <td class="custom-table__body-td">
                <a href="student-profile.php" class="custom-table__body-text">Setyo Syahindra</a>
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td--small">
                Topic 1 : Whats's coding
                <div class="progress progress-custom--submission">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 70%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Active
                </span>
              </td>
              <td class="custom-table__body-td text-center">
                <div class="btn-group btn-action-wrap">
                  <button type="button" class="btn btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <i class="fas fa-ellipsis-h fa-lg text-secondary"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-custom dropdown-action">
                    <a class="dropdown-item dropdown-action__item" href="">Contact Student</a>
                    <a class="dropdown-item dropdown-action__item" href="student-profile.php">Progress Detail</a>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <a href="student-profile.php" class="custom-table__body-text">Setyo Syahindra</a>
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td--small">
                Topic 1 : Whats's coding
                <div class="progress progress-custom--submission">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Active
                </span>
              </td>
              <td class="custom-table__body-td text-center">
                <div class="btn-group btn-action-wrap">
                  <button type="button" class="btn btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <i class="fas fa-ellipsis-h fa-lg text-secondary"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-custom dropdown-action">
                    <a class="dropdown-item dropdown-action__item" href="">Contact Student</a>
                    <a class="dropdown-item dropdown-action__item" href="">Progress Detail</a>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <a href="student-profile.php" class="custom-table__body-text">Setyo Syahindra</a>
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td--small">
                Topic 1 : Whats's coding
                <div class="progress progress-custom--submission">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 30%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Active
                </span>
              </td>
              <td class="custom-table__body-td text-center">
                <div class="btn-group btn-action-wrap">
                  <button type="button" class="btn btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <i class="fas fa-ellipsis-h fa-lg text-secondary"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-custom dropdown-action">
                    <a class="dropdown-item dropdown-action__item" href="">Contact Student</a>
                    <a class="dropdown-item dropdown-action__item" href="">Progress Detail</a>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <a href="student-profile.php" class="custom-table__body-text">Setyo Syahindra</a>
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td--small">
                Topic 1 : Whats's coding
                <div class="progress progress-custom--submission">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 70%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Active
                </span>
              </td>
              <td class="custom-table__body-td text-center">
                <div class="btn-group btn-action-wrap">
                  <button type="button" class="btn btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <i class="fas fa-ellipsis-h fa-lg text-secondary"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-custom dropdown-action">
                    <a class="dropdown-item dropdown-action__item" href="">Contact Student</a>
                    <a class="dropdown-item dropdown-action__item" href="">Progress Detail</a>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <a href="student-profile.php" class="custom-table__body-text">Setyo Syahindra</a>
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td--small">
                Topic 1 : Whats's coding
                <div class="progress progress-custom--submission">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Active
                </span>
              </td>
              <td class="custom-table__body-td text-center">
                <div class="btn-group btn-action-wrap">
                  <button type="button" class="btn btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <i class="fas fa-ellipsis-h fa-lg text-secondary"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-custom dropdown-action">
                    <a class="dropdown-item dropdown-action__item" href="">Contact Student</a>
                    <a class="dropdown-item dropdown-action__item" href="">Progress Detail</a>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <a href="student-profile.php" class="custom-table__body-text">Setyo Syahindra</a>
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td--small">
                Topic 1 : Whats's coding
                <div class="progress progress-custom--submission">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 90%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Active
                </span>
              </td>
              <td class="custom-table__body-td text-center">
                <div class="btn-group btn-action-wrap">
                  <button type="button" class="btn btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <i class="fas fa-ellipsis-h fa-lg text-secondary"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-custom dropdown-action">
                    <a class="dropdown-item dropdown-action__item" href="">Contact Student</a>
                    <a class="dropdown-item dropdown-action__item" href="">Progress Detail</a>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <a href="student-profile.php" class="custom-table__body-text">Setyo Syahindra</a>
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td--small">
                Topic 1 : Whats's coding
                <div class="progress progress-custom--submission">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 70%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Active
                </span>
              </td>
              <td class="custom-table__body-td text-center">
                <div class="btn-group btn-action-wrap">
                  <button type="button" class="btn btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <i class="fas fa-ellipsis-h fa-lg text-secondary"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-custom dropdown-action">
                    <a class="dropdown-item dropdown-action__item" href="">Contact Student</a>
                    <a class="dropdown-item dropdown-action__item" href="">Progress Detail</a>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <a href="student-profile.php" class="custom-table__body-text">Setyo Syahindra</a>
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td--small">
                Topic 1 : Whats's coding
                <div class="progress progress-custom--submission">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 40%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Active
                </span>
              </td>
              <td class="custom-table__body-td text-center">
                <div class="btn-group btn-action-wrap">
                  <button type="button" class="btn btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <i class="fas fa-ellipsis-h fa-lg text-secondary"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-custom dropdown-action">
                    <a class="dropdown-item dropdown-action__item" href="">Contact Student</a>
                    <a class="dropdown-item dropdown-action__item" href="">Progress Detail</a>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <a href="student-profile.php" class="custom-table__body-text">Setyo Syahindra</a>
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td--small">
                Topic 1 : Whats's coding
                <div class="progress progress-custom--submission">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 20%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle fa-sm mr-2 text-green"></i>
                  Active
                </span>
              </td>
              <td class="custom-table__body-td text-center">
                <div class="btn-group btn-action-wrap">
                  <button type="button" class="btn btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                    <i class="fas fa-ellipsis-h fa-lg text-secondary"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-custom dropdown-action">
                    <a class="dropdown-item dropdown-action__item" href="">Contact Student</a>
                    <a class="dropdown-item dropdown-action__item" href="">Progress Detail</a>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>

        <div class="pagination-custom">
          <a href="" class="pagination-custom__item"><i class="fas fa-chevron-left"></i></a>
          <a href="" class="pagination-custom__item active">1</a>
          <a href="" class="pagination-custom__item">2</a>
          <a href="" class="pagination-custom__item">3</a>
          <a href="" class="pagination-custom__item">4</a>
          <a href="" class="pagination-custom__item">5</a>
          <a href="" class="pagination-custom__item"></a>
          <a href="" class="pagination-custom__item">15</a>
          <a href="" class="pagination-custom__item"><i class="fas fa-chevron-right"></i></a>
        </div>
      </section>

      <!-- back to top -->
      <button class="btn-back-to-top">
        <i class="fas fa-angle-double-up"></i>
      </button>
    </div>
  </main>

  <?php require_once "../component/mentor/footer.php" ?>

  <!-- Bootstrap -->
  <script src="../js/jquery-3.5.1.slim.min.js"></script>
  <script src="../js/bootstrap.bundle.min.js"></script>

  <!-- Select picker -->
  <script src="../js/bootstrap-select.js"></script>

  <!-- Layout -->
  <script src="../js/pages/layout.js"></script>

  <!-- This Page Only JS -->
  <script src="../js/pages/dashboard.js"></script>
</body>

</html>