<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap"
    rel="stylesheet" />
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet" />

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="../css/layout.css" />

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="../css/pages/account.css" />

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="../img/faveicon/timedoor-faveicon.jpg" />

  <title>Students Profile - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "../component/mentor/header.php" ?>

  <main>
    <div class="container">
      <section class="account">
        <h2 class="section__title account-info__title">Student's Profile</h2>
        <div class="row">
          <div class="col-12">
            <div class="account__left">
              <div class="row justify-content-between">
                <div class="col-2">
                  <img src="../img/student-img.png" alt="User" class="img-thumbnail account-info__image" />
                </div>
                <div class="col-6">
                  <h1 class="section__title account-info__title">
                    Setyo Syahindra
                    <i class="fas fa-check-circle fa-sm text-green"></i>
                  </h1>
                  <p class="account__desc--small text-black-50">
                    <img src="../img/icon/icon-birthday.svg" alt="Birthday" class="mr-2 align-text-top" />12 Dec / Male
                  </p>
                  <div class="row">
                    <div class="col-6">
                      <p class="account__desc">
                        <img src="../img/icon/icon-tel.svg" alt="Phone" class="mr-2" />+62 87 335 445 888
                      </p>
                    </div>
                    <div class="col-6">
                      <p class="account__desc">Joined Date :</p>
                    </div>
                    <div class="col-6">
                      <p class="account__desc">
                        <img src="../img/icon/icon-location.svg" alt="province" class="mr-2" />Denpasar, Bali
                      </p>
                    </div>
                    <div class="col-6">
                      <p class="account__desc">10 October 2021 / 7:33 PM</p>
                    </div>
                  </div>
                </div>
                <div class="col-4 text-right">
                  <a href="account-info.php" class="btn btn-dark mt-5 w-75">
                    Contact Student
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="overview">
        <h2 class="section__title">Learning Overview</h2>
        <div class="row">
          <div class="col-8">
            <div class="overview__left">
              <p class="section__title--small mb-2">
                <i class="fas fa-circle fa-sm mr-2 text-warning"></i> On Going
                Course
              </p>
              <h3 class="overview__title">
                Chapter 2 - Basic HTML and CSS
                <span class="overview__title-date">10-12-2021 / 9:30 PM</span>
              </h3>
              <p class="overview__subtitle">Topic 1 : Show Content : Text</p>
              <a href="" class="btn btn-dark--small">
                <i class="fas fa-code mr-2"></i> Project
              </a>
              <span class="overview__title-date ml-2">Coding Submission</span>
              <div class="progress progress-custom">
                <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 70%" aria-valuenow="50"
                  aria-valuemin="0" aria-valuemax="100"></div>
                <span class="progress-custom__percent">70%</span>
              </div>
            </div>
          </div>
          <div class="col-4 pl-0 d-flex align-items-stretch">
            <div class="overview__right">
              <h3 class="overview__title mb-4">Student Badge</h3>
              <i class="fas fa-star overview__badge"></i>
              <span class="overview__badge-text">Good</span>
              <img src="../img/icon/Icon-feather-info.svg" alt="Info" data-toggle="modal" data-target="#exampleModal"
                class="overview__badge-info" />
              <h3 class="overview__title mt-4">Overall Score</h3>
              <span class="overview__score">85</span>
            </div>
          </div>
        </div>
      </section>

      <section class="submission mb-4">
        <div class="submission__head">
          <h1 class="section__title submission__title">
            Setyo's Submission List
          </h1>
        </div>
        <table class="submision__table custom-table">
          <thead>
            <tr>
              <th class="custom-table__head-td" width="330px">Submission Title</th>
              <th class="custom-table__head-td" width="170px">Start Date</th>
              <th class="custom-table__head-td" width="170px">Finished</th>
              <th class="custom-table__head-td" width="160px">Status</th>
              <th class="custom-table__head-td" width="160px">Review By</th>
              <th class="custom-table__head-td" width="230px">Badge</th>
            </tr>
          </thead>
          <tbody class="custom-table__body">
            <tr>
              <td class="custom-table__body-td">
                <b>Ch.1</b> - Final Exam
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td">Mr. Bill Gates</td>
              <td class="custom-table__body-td">
                <span class="badge badge-green w-50">Excellent</span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <b>Ch.2</b> - Final Exam
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td">Ms. Wanda</td>
              <td class="custom-table__body-td">
                <span class="badge badge-darkgreen w-50">Good</span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <b>Ch.3</b> - Final Exam
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-warning"></i>
                  In revision
                </span>
              </td>
              <td class="custom-table__body-td">Ms. Wanda</td>
              <td class="custom-table__body-td">
                <span class="badge badge-yellow w-50">Try Again</span>
              </td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <b>Ch.2</b> - Topic 2 (Starting HTML)
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td">Ms. Gita</td>
              <td class="custom-table__body-td--muted">Not Available</td>
            </tr>
            <tr>
              <td class="custom-table__body-td">
                <b>Ch.1</b> - Topic 1 (Basic Thinking)
              </td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">10-10-2021</td>
              <td class="custom-table__body-td">
                <span class="d-flex align-items-baseline">
                  <i class="fas fa-circle mr-1 text-green"></i>
                  Finished
                </span>
              </td>
              <td class="custom-table__body-td">Mr. Bill Gates</td>
              <td class="custom-table__body-td--muted">Not Available</td>
            </tr>
          </tbody>
        </table>
      </section>
    </div>

    <!-- back to top -->
    <button class="btn-back-to-top">
      <i class="fas fa-angle-double-up"></i>
    </button>
  </main>

  <!-- Modal -->
  <div class="modal fade modal-custom" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content modal-custom">
        <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-times custom-close__button"></i></span>
        </button>
        <div class="modal-body text-center">
          <h3 class="modal-custom__title">Student Badge</h3>
          <p class="modal-custom__desc">
            Score Range
          </p>
          <div class="modal-custom__badge-container">
            <div class="modal-custom__badge">
              <img src="../img/icon/icon-star-green.svg" alt="Info" class="" />
              <p class="modal-custom__badge-desc--green">Excellent</p>
              <p class="modal-custom__score">Score : 86 - 100</p>
            </div>
            <div class="modal-custom__badge">
              <img src="../img/icon/icon-star-darkgreen.svg" alt="Info" class="" />
              <p class="modal-custom__badge-desc--darkgreen">Good</p>
              <p class="modal-custom__score">Score : 70 - 85</p>
            </div>
            <div class="modal-custom__badge">
              <img src="../img/icon/icon-star-orange.svg" alt="Info" class="" />
              <p class="modal-custom__badge-desc--orange">Learn More</p>
              <p class="modal-custom__score">Score : 0 - 69</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php require_once "../component/mentor/footer.php" ?>

  <!-- Bootstrap -->
  <script src="../js/jquery-3.5.1.slim.min.js"></script>
  <script src="../js/bootstrap.bundle.min.js"></script>

  <!-- Layout js -->
  <script src="../js/pages/layout.js"></script>
</body>

</html>