<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@300&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="../css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="../css/pages/final-exam-review.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="../img/faveicon/timedoor-faveicon.jpg">

  <title>Final Exam Review - Timedoor Coding Academy</title>
</head>

<body>
  <?php require_once "../component/mentor/header.php" ?>
  <section class="header-chapter">
    <div class="container container-custom">
      <div class="header-chapter__container">
        <div class="header-chapter__left">
          <a href="" class="btn-back"><i class="fas fa-angle-left fa-lg"></i></a>
          <h1 class="header-chapter__title">Final Exam Review</h1>
        </div>
        <div class="header-chapter__right">
          <div class="header-chapter__right-wrapper">
            <p class="header-chapter__subtitle d-block">Chapter 2 : Meeting 1 ( Show Content : Text )</p>
            <p class="header-chapter__title d-block text-right">Brown Bear Article</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <main>
    <section class="coding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-8 px-0">
            <div class="coding__left editor">
              <?php require_once "../component/sidebar-editor.php" ?>

              <!-- Editor Content -->
              <div class="editor__content">
                <div class="editor__head">
                  <ul class="editor__head-items">
                    <li class="editor__head-item active">HTML</li>
                    <li class="editor__head-item">CSS</li>
                    <li class="editor__head-item">Javascript</li>
                  </ul>
                </div>
                <div class="editor__text-wrapper">
                  <div id="editor">function foo(items) {
                    var x = "All this is syntax highlighted";
                    return x;
                    }
                  </div>
                  <!-- Overlay -->
                  <div class="overlay">

                    <div class="overlay__score">
                      <i class="fas fa-times overlay__score-close"></i>
                      <span class="overlay__title">Input Score</span>

                      <table width="45%" class="mx-auto">
                        <tr>
                          <td>
                            <span class="overlay__score-text--green">Excellent</span>
                          </td>
                          <td>
                            <span class="overlay__score-points">86 - 100</span>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <span class="overlay__score-text--darkgreen">Good</span>
                          </td>
                          <td>
                            <span class="overlay__score-points">70 - 85</span>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <span class="overlay__score-text--orange">Learn More</span>
                          </td>
                          <td>
                            <span class="overlay__score-points">0 - 69</span>
                          </td>
                        </tr>
                      </table>
                      <div class="form-group mt-4">
                        <input type="number" class="form-control form__input w-75 mx-auto" id="score">
                      </div>
                      <button type="submit" class="btn btn-dark--small d-block mx-auto w-25">Submit</button>
                    </div>

                  </div>
                </div>
                <div class="editor__footer">
                  <span class="editor__footer-text">
                    Preview
                  </span>
                  <span class="editor__footer-text">Student Badge
                    <img src="../img/icon/icon-star-green.svg" alt="Excellent" width="18px" height="18px"
                      class="editor__footer-badge" id="btn-badge">
                    <img src="../img/icon/icon-star-darkgreen.svg" alt="Good" width="18px" height="18px"
                      class="editor__footer-badge">
                    <img src="../img/icon/icon-star-orange.svg" alt="Try Again" width="18px" height="18px"
                      class="editor__footer-badge">
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-4 px-0">
            <div class="coding__right">
              <!-- Accordion -->
              <div class="accordion" id="accordionExample">

                <div class="card border-0">
                  <div class="card-header coding__right-title p-0" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn-requirement collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Project Requirement
                      </button>
                    </h2>
                  </div>

                  <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body coding__right-container">
                      <div class="coding__right-head">
                        <h3 class="coding__right-subtitle">
                          Brown Bear Text Article</h3>
                      </div>
                      <p class="coding__right-desc">
                        Write an article about Brown Bear. The title is The Brown Bear. In this article, there are
                        several
                        sections as follows:
                        <br>1. About Brown Bear <br>2. Species <br>3. Features <br>4. Habitat <br>5. Media <br><br>Each
                        section
                        uses
                        sub headings, and if there are subsections in it you can use the next sub heading. The species
                        section
                        uses an unordered list while the habitat uses an ordered list with numeric types. Add photos and
                        videos about Brown Bears to the media section. Don't forget to use bold, italic, and underline
                        text if
                        needed.
                      </p>
                    </div>
                  </div>
                </div>
                <div class="card border-0">
                  <div class="card-header coding__right-title p-0" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn-requirement" type="button" data-toggle="collapse" data-target="#collapseTwo"
                        aria-expanded="true" aria-controls="collapseTwo">
                        Mentor's Comment
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionExample">
                    <div class="card-body coding__right-container d-flex justify-content-center align-items-center">
                      <p class="coding__right-desc">

                      </p>
                      <div class="comment">
                        <div class="form-group mb-0 w-75">
                          <input type="text" class="form-control comment__input" id="comment">
                        </div>
                        <button type="submit" class="btn btn-dark--small">Comment</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Accordion -->
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <!-- Bootstrap -->
  <script src="../js/jquery-3.5.1.slim.min.js"></script>
  <script src="../js/bootstrap.bundle.min.js"></script>

  <!-- Ace Editor -->
  <script src="../js/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>

  <!-- This Page JS -->
  <script src="../js/pages/final-exam-review.js"></script>

</body>

</html>