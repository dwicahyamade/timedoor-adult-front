<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/account.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Account Info - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "component/header.php" ?>

  <main>
    <section class="account">
      <div class="container">
        <h1 class="section__title">
          <a href="dashboard.php" class="btn-back"><i class="fas fa-angle-left"></i></a>
          Account Settings
        </h1>
        <div class="row">
          <div class="col-lg-4 account-navigation">
            <div class="section__bg account-setting__bg h-100">
              <div class="account-navigation__wrapper">
                <a href="account-info.php" class="account-navigation__item">
                  <i class="far fa-user mr-3"></i> User Info</a>
                <a href="account-security.php" class="account-navigation__item active">
                  <img src="img/icon/icon-lock.svg" alt="Security" class="mr-3"> Security</a>
                <a href="account-delete.php" class="account-navigation__item">
                  <img src="img/icon/icon-delete-account.svg" alt="Delete Account" class="mr-2"> Delete Account</a>
                <a href="index.php" class="account-navigation__item--green ">
                  <i class="fas fa-sign-out-alt mr-2"></i> Log out</a>
              </div>
            </div>
          </div>
          <div class="col-lg-8 mt-3 mt-lg-0 pl-lg-0 account-info">
            <div class="section__bg account-setting__bg">
              <div class="alert alert-custom alert-custom__success" role="alert">
                Successfully change your password!
              </div>
              <h1 class="section__title account-info__title">Change Password</h1>
              <p class="account-info__desc">We requires that you have a secure password. If your password is too weak,
                make sure you use at
                least 6 characters (using uppercase & lowercase), combination of numbers or characters like .”, “&”,
                or “*”</p>
              <form class="form">
                <div class="row">
                  <div class="col-lg-7">
                    <div class="form-group position-relative mb-3">
                      <label for="password" class="form__title px-1">
                        <img src="img/icon/icon-lock.svg" alt="Password" class="mr-2"> Current Password</label>
                      </label>
                      <input type="password" class="form-control form__input" id="password">
                    </div>
                    <div class="form-group position-relative mb-3">
                      <label for="password" class="form__title px-1">
                        <img src="img/icon/icon-lock.svg" alt="Password" class="mr-2"> New Password</label>
                      </label>
                      <input type="password" class="form-control form__input" id="confirmPassword">
                    </div>
                    <button type="button" class="btn btn-dark w-100 btn-save">Save Changes</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <?php require_once "component/footer.php" ?>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Custom -->
  <script src="js/pages/account.js"></script>
</body>

</html>