<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/course-subscription.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Subscribe Us - Timedoor Coding Academy</title>
</head>

<body class="body-subscribe">

  <?php require_once 'component/header.php' ?>

  <!-- <header class="nav-custom">
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container container-custom">
				<a class="navbar-brand" href="dashboard.php"><img src="img/timedoor-logo-black.png" alt="Timedoor Logo" width="141" height="33"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
				<div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-2 my-1">
              <a href="sign-up.php" class="btn btn-dark btn-start-free-trial mr-2 w-100" type="submit">Start Free Trial</a>
            </li>
            <li class="nav-item mx-2 my-1">
              <a href="index.php" class="btn btn-outline-dark h-100 w-100" type="submit">Log In</a>
            </li>
            <li class="nav-item">
              <div class="btn-group btn-language w-100">
                <button type="button" class="btn btn-transparent" data-toggle="dropdown" aria-haspopup="true"
                  aria-expanded="false">
                  English
                  <i class="fas fa-chevron-down fa-sm arrow-down"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-custom-nav position-absolute">
                  <a class="dropdown-item dropdown-custom-nav__item" href="">English</a>
                  <a class="dropdown-item dropdown-custom-nav__item" href="">Indonesia</a>
                </div>
              </div>
            </li>
          </ul>
				</div>
			</div>
		</nav>
	</header> -->

  <main>

    <section class="section heading">
      <div class="container">
        <div class="row">
          <div class="col-9 col-sm-8 col-lg-6">
            <h1 class="heading__title">Berlangganan dan Belajar <span class="heading__title--green">Lebih Banyak</span></h1>
          </div>
        </div>
      </div>
    </section>

    <section class="section">
      <div class="container">
        <div class="mentor">
          <div class="row justify-content-end">
            <div class="col-lg-5 order-2 order-lg-1">
              <h2 class="section__title mentor__title">Belajar Coding Bersama Mentor Profesional</h2>
              <p class="section__desc mentor__desc">
                Mentor Timedoor Pro Code sudah berpengalaman di bidang teknologi khususnya programming. Jadi jika kamu mengalami kesulitan selama belajar atau saat membuat project pribadimu, jangan ragu untuk bertanya pada mentor. Mentor akan sigap membantu menyelesaikan permasalah yang ada.
              </p>
            </div>
            <div class="col-lg-6 order-1 order-lg-1">
              <div class="mentor__image">
                <img src="img/subscription-course/hero-img.svg" alt="Belajar Coding Bersama Mentor Profesional" width="100%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section how-to">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="section__title how-to__title">
              Bagaimana Mentor Mengajar di Timedoor Pro Coder?
            </h2>
          </div>
          <div class="col-lg-6 mb-5">
            <div class="how-to__image">
              <img src="img/subscription-course/how-to-review.svg" alt="Code Review Oleh Programmer Profesional" width="90%">
            </div>
          </div>
          <div class="col-lg-6 mb-5">
            <h3 class="how-to__subtitle">Code Review Oleh Programmer Profesional</h3>
            <p class="section__desc how-to__desc">
              Mentor Timedoor Pro Coder adalah programmer expert dibidangnya dan berita baiknya project pribadimu akan langsung di review oleh mentor kami
            </p>
          </div>
          <div class="col-lg-6 mb-0 mb-lg-5 order-2 order-lg-0">
            <h3 class="how-to__subtitle">Konsultasi Lewat Chat Kapan Dan Dimana Saja</h3>
            <p class="section__desc how-to__desc">
              Jika kamu menemukan masalah selama belajar, berkonsultasi dengan mentor kami adalah solusi terbaik. Mentor siap berkonsultasi tentang course pilihanmu kapan saja dan dimana saja.
            </p>
          </div>
          <div class="col-lg-6 mb-5 mb-lg-0 order-1 order-lg-0">
            <div class="how-to__image">
              <img src="img/subscription-course/how-to-chat.svg" alt="Konsultasi Lewat Chat Kapan Dan Dimana Saja" width="90%">
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section curriculum">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6 d-none d-lg-block">
            <div class="curriculum__image">
              <img src="img/subscription-course/curriculum-1.svg" alt="Kurikulum Kami" width="90%">
            </div>
          </div>
          <div class="col-lg-6">
            <h2 class="section__title curriculum__title">Kurikulum Kami</h2>
            <p class="section__desc curriculum__desc">Kurikulum Timedoor Pro Coder dibuat berdasarkan kebutuhan siswa. Agar setelah mengambil course, siswa dapat langsung membuat project sendiri.</p>
            <div class="curriculum__image">
              <img src="img/subscription-course/curriculum-2.svg" alt="Kurikulum Kami" width="100%">
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section schedule">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-7">
            <h2 class="section__title schedule__title">Jadwal Course Dan Fase Study (Waktu Belajar)</h2>
          </div>
          <div class="col-lg-6 mb-0 mb-lg-5">
            <div class="schedule__image-container">
              <picture>
                <source media="(max-width:425px)" srcset="img/subscription-course/PROGRAM-3-BULAN-MOBILE.svg">
                <img src="img/subscription-course/PROGRAM-3-BULAN.svg" alt="Table Program 3 Bulan" class="schedule__image" width="100%">
              </picture>
            </div>
          </div>
          <div class="col-lg-6 mb-5 pt-5">
            <p class="schedule__recomend-text"><img src="img/icon/icon-check-green.svg" alt="Disarankan"> Disarankan</p>
            <h3 class="schedule__subtitle">1 jam di hari kerja, <br> 3 jam di akhir pekan</h3>
            <p class="schedule__desc">
              Jika kamu saat ini sedang bekerja atau menempuh pendidikan, kamu tetap bisa mengikuti course ini. 
            </p>
            <p class="schedule__desc">
              <i class="fa fa-circle text-green schedule__dot"></i> Belajar Course pilihanmu 1 jam dalam sehari saat kamu selesai bekerja atau sekolah/ Saat hari kerja, belajar course pilihanmu 1 jam sehari
            </p>
            <p class="schedule__desc">
              <i class="fa fa-circle text-green schedule__dot"></i> Saat Akhir Pekan belajar course pilihanmu selama 3 jam sehari
            </p>
            <p class="schedule__desc mb-5">
              <i class="fa fa-circle text-green schedule__dot"></i> Mentor akan memantau dan membantumu selama proses belajar mandiri ini
            </p>
            <div class="row">
              <div class="col-6 pr-2">
                <a href="#" class="btn schedule__btn-dark">Web Development</a>
              </div>
              <div class="col-6 pl-2">
                <a href="#" class="btn schedule__btn-grey">Digital Business Expert</a>
              </div>
            </div>
          </div>

          <div class="col-lg-6 mb-5 pt-5 order-2 order-lg-1">
            <h3 class="schedule__subtitle">Setiap hari, 3 jam kerja</h3>
            <p class="schedule__desc">
              Jika sedang tidak sibuk, kamu bisa menyelesaikan course pilihanmu dalam 1 bulan saja. 
            </p>
            <p class="schedule__desc">
              <i class="fa fa-circle text-green schedule__dot"></i> Cukup belajar course pilihanmu selama 3 jam sehari
            </p>
            <p class="schedule__desc mb-5">
              <i class="fa fa-circle text-green schedule__dot"></i> Mentor akan memantau dan membantumu selama proses belajar mandiri ini 
            </p>
            <div class="row">
              <div class="col-6 pr-2">
                <a href="#" class="btn schedule__btn-dark">Web Development</a>
              </div>
              <div class="col-6 pl-2">
                <a href="#" class="btn schedule__btn-grey">Digital Business Expert</a>
              </div>
            </div>
          </div>
          <div class="col-lg-6 order-1 order-lg-1">
            <div class="schedule__image-container">
              <picture>
                <source media="(max-width:425px)" srcset="img/subscription-course/PROGRAM-1-BULAN-MOBILE.svg">
                <img src="img/subscription-course/PROGRAM-1-BULAN.svg" alt="Table Program 1 Bulan" class="schedule__image" width="100%">
              </picture>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section subscribe">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="section__title subscribe__title">Cara Berlangganan Course</h2>
          </div>
          <div class="col-lg-4">
            <div class="subscribe__step">
              <div class="subscribe__image">
                <img src="img/subscription-course/icon-subscribe-1.svg" alt="Konsultasi Dengan Mentor">
              </div>
              <h3 class="subscribe__step-title">Konsultasi Dengan Mentor</h3>
              <p class="subscribe__step-desc">Konsultasikan Seputar Course Secara Personal Dengan Mentor Kami</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="subscribe__step">
              <div class="subscribe__image">
                <img src="img/subscription-course/icon-subscribe-2.svg" alt="Lakukan Pembayaran Untuk Mendapatkan Courses Pilihanmu">
              </div>
              <h3 class="subscribe__step-title">Lakukan Pembayaran</h3>
              <p class="subscribe__step-desc">Lakukan Pembayaran Untuk Mendapatkan Courses Pilihanmu</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="subscribe__step">
              <div class="subscribe__image">
                <img src="img/subscription-course/icon-subscribe-3.svg" alt="Mulai Course Pilihanmu">
              </div>
              <h3 class="subscribe__step-title">Mulai Course Pilihanmu</h3>
              <p class="subscribe__step-desc">Kini kamu dapat mengakses courses pilihanmu kapan saja di mana saja</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section payment">
      <div class="container">
        <div class="row justify-content-end">
          <div class="col-12 col-lg-8">
            <h2 class="section__title">
              Pilih Metode Pembayaran
            </h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 mb-4 mb-lg-0">
            <div class="payment__image">
              <img src="img/subscription-course/payment.svg" alt="Payment" width="90%">
            </div>
          </div>
          <div class="col-lg-8">
            <div class="row">
              <div class="col-12">
                <p class="section__desc payment__desc">
                  Timedoor Pro Coder menyediakan berbagai metode pembayaran untuk memudahkanmu mengambil course pilihan
                </p>
              </div>
              <div class="col-12 order-3 order-lg-1">
                <div class="payment__choices">
                  <div class="row">
                    <div class="col-lg-3 mb-2">
                      <img src="img/subscription-course/midtrans-logo.svg" alt="Midtrans">
                    </div>
                    <div class="col-lg-9 mb-2">
                      <div class="row">
                        <div class="col-12 col-sm-6">
                          <div class="payment__choices-item">Debit online / kartu kredit</div>
                        </div>
                        <div class="col-12 col-sm-6">
                          <div class="payment__choices-item">ATM / Tranfer bank</div>
                        </div>
                        <div class="col-12 col-sm-6">
                          <div class="payment__choices-item">E - Wallets (Dompet digital)</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 order-2 order-lg-1">
                <div class="payment__price-container">
                  <img src="img/landing-page/icon-best-price.png" alt="Best Price" class="payment__best-deal">
                  <h3 class="payment__price">IDR 1,000,<span class="payment__price--small">000,-</span><span class="payment__price--month"> /4 Bulan</span>
                  </h3>
                </div>
              </div>
              <div class="col-12 order-4 order-lg-1">
                <div class="row">
                  <div class="col-12 col-md-6 col-lg-5 mb-3 mb-lg-0">
                    <a href="#" class="btn payment__btn">Bayar Sekarang</a>
                  </div>
                </div>
              </div>
            </div>            
          </div>
        </div>
      </div>
    </section>

    <section class="section">
      <div class="faq">
        <div class="container">
          <h2 class="section__title faq__title">FAQ</h2>

          <div class="accordion faq__accordion" id="accordionFeature">

            <div class="faq__card">
              <div class="faq__card-header" id="headingOne">
                <h3 class="mb-0">
                  <button class="btn faq__accordion-title" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Bagaimana Cara Mengambil Course?
                    <span class="faq__accordion-icon"></span>
                  </button>
                </h3>
              </div>
              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFeature">
                <div class="faq__accordion-body">
                  Ikuti trial gratis dari kami. Kemudian konsultasikan dengan mentor jika kamu tertarik, lakukan pembayaran, dan mulai belajar course pilihanmu.
                </div>
              </div>
            </div>

            <hr class="faq__divider">

            <div class="faq__card">
              <div class="faq__card-header" id="headingTwo">
                <h3 class="mb-0">
                  <button class="btn faq__accordion-title collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Bentuk Materi Pembelajaran Course?
                    <span class="faq__accordion-icon"></span>
                  </button>
                </h3>
              </div>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionFeature">
                <div class="faq__accordion-body">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla similique libero consequatur rerum fugit quisquam.
                </div>
              </div>
            </div>

            <hr class="faq__divider">

            <div class="faq__card">
              <div class="faq__card-header" id="headingThree">
                <h3 class="mb-0">
                  <button class="btn faq__accordion-title collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Cara Kerja Coding Practice Dengan Code Editor?
                    <span class="faq__accordion-icon"></span>
                  </button>
                </h3>
              </div>
              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionFeature">
                <div class="faq__accordion-body">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam corporis eaque labore facilis, eveniet incidunt qui minima adipisci nam illum.
                </div>
              </div>
            </div>

            <hr class="faq__divider">

            <div class="faq__card">
              <div class="faq__card-header" id="headingFour">
                <h3 class="mb-0">
                  <button class="btn faq__accordion-title collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    Cara Menghubungi Mentor?
                    <span class="faq__accordion-icon"></span>
                  </button>
                </h3>
              </div>
              <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionFeature">
                <div class="faq__accordion-body">
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolores, veritatis amet expedita cupiditate commodi quasi eum ad tenetur?
                </div>
              </div>
            </div>

            <hr class="faq__divider">

            <div class="faq__card">
              <div class="faq__card-header" id="headingFive">
                <h3 class="mb-0">
                  <button class="btn faq__accordion-title collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    Bagaimana Mentor Membantu Kamu Menyelesaikan Course?
                    <span class="faq__accordion-icon"></span>
                  </button>
                </h3>
              </div>
              <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionFeature">
                <div class="faq__accordion-body">
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Vero ad eius odio, explicabo, veritatis architecto dolores ratione voluptate cupiditate suscipit nisi maiores?
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </section>

    <section class="section terms">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12">
            <h2 class="section__title terms__title">Syarat & Ketentuan</h2>
          </div>
          <div class="col-lg-9">
            <p class="section__desc terms__desc">Ketahui dan Pahami Syarat dan Ketentuan Layanan dari Timedoor Pro Coder untuk kenyamanan selama menyelesaikan Course Pilihanmu</p>
          </div>
          <div class="col-lg-6 text-center">
            <a href="privacy-terms.php" class="btn terms__btn">Lihat Syarat & Ketentuan</a>
          </div>
        </div>
      </div>
    </section>

    <section class="section contact">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <img src="img/timedoor-logo-black.png" alt="Timedoor" width="184" height="auto">
          </div>
          <div class="col-sm-6 col-lg-3">
            <a href="mailto:info@timedoor.net" class="contact__text"><img src="img/landing-page/icon-mail.svg" alt="Mail Us" class="mr-2"> info@timedoor.net</a>
          </div>
          <div class="col-lg-6">
            <p class="contact__text"><img src="img/landing-page/icon-location.svg" alt="Location" class="mr-2"> Jl. Tukad Yeh Aya IX No.46, Renon , Denpasar, Bali</p>
          </div>
        </div>
      </div>
    </section>

  </main>

  <footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<p class="footer__text">Timedoor Coding Academy Adult ©2021 PT. Timedoor Indonesia All Right Reserved</p>
				</div>
				<div class="col-lg-5 text-center text-lg-right">
					<a href="privacy-terms.php" class="footer__text">Privacy policy</a> 
					<span class="footer__text">|</span>
					<a href="privacy-terms.php" class="footer__text">Terms of service</a>
				</div>
			</div>
		</div>
	</footer>

  
  <!-- Back to top -->
  <?php require_once 'component/button-back-to-top.php' ?>

  <!-- Layout -->
  <script src="js/pages/layout.js"></script>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>