<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/chapter-opening.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Chapter 1 - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "component/header.php" ?>
  <main>
    <!-- <div class="container">
      <section class="chapter">
        <div class="chapter__bg">
          <h1 class="chapter__title">Free Trial Course</h1>
          <h2 class="chapter__subtitle">Introduction</h2>
          <p class="chapter__desc">You will learn about basic part of making a website!</p>
          <a href="chapter-opening.php" class="btn btn-dark w-50">Start Learning</a>
        </div>
      </section>
    </div> -->
    <section class="chapter">

      <div class="container">
        <div class="row">
          <div class="col-4">
            <div class="chapter__robot">
              <img src="img/robot/robot-wave.svg" alt="Robot" class="chapter__robot-image active">
              <img src="img/robot/robot-idle.svg" alt="Robot" class="chapter__robot-image hidden" >
              <img src="img/robot/robot-wave-2.svg" alt="Robot" class="chapter__robot-image hidden" >
            </div>
          </div>
          <div class="col-8">
            <h1 class="chapter__title">The Introduction</h1>
            <div class="chapter__bg">
              <div class="chapter__content active">
                <h2 class="chapter__subtitle--green mb-5">Hello! I'm Robot. <br>
                <span class="chapter__subtitle">I will help you with this trial</span></h2>
                <p class="chapter__desc">percakapan umum dengan student, bagaimana apakah siap belajar coding? Penjelasan singkat tentang course</p>
              </div>

              <div class="chapter__content hidden">
                <h2 class="chapter__subtitle">What will we learn <br>in this trial?</h2>
                <p class="chapter__desc mb-5">This topic will contain material about introduction to the internet. Material includes <b>what is the internet, history, and the opportunities of the internet.</b></p>

                <p class="chapter__desc--green">
                  Tip : This is good to test your basic thinking and give you a big picture about coding and our teaching system
                </p>
              </div>

              <div class="chapter__content hidden text-center">
                <h2 class="chapter__subtitle">Are You Ready?</h2>
                <p class="chapter__desc">Lets get started!</p>
                <a href="learning-video.php" class="btn btn-dark--wide">Start Learning</a>
              </div>
              
            </div>
            <div class="chapter__button-container">
              <span class="chapter__prev">Prev</span>
              <span class="chapter__next ml-2">Next</span>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- CUstom JS -->
  <script src="js/pages/chapter-opening-trial.js"></script>

</body>

</html>