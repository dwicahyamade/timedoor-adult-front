<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/login.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Login - Timedoor Coding Academy</title>
</head>

<body class="body-bg">
  <header>
    <div class="header py-3">
      <div class="container">
        <nav class="navbar navbar-dark bg-transparent">
          <a class="navbar-brand" href="#">
            <img src="img/timedoor logo white@2x.png" width="168" height="39" alt="Timedoor Logo">
          </a>
          <div class="my-2 my-lg-0">
            <a href="sign-up.php" class="btn btn-outline-dark my-2 my-sm-0" type="submit">Sign Up</a>
            <div class="btn-group btn-language">
              <button type="button" class="btn btn-transparent" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
                English
                <i class="fas fa-chevron-down fa-sm arrow-down"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right dropdown-custom-nav">
                <a class="dropdown-item dropdown-custom-nav__item" href="">English</a>
                <a class="dropdown-item dropdown-custom-nav__item" href="">Indonesia</a>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </header>
  <main>
    <section class="banner">
      <div class="container">
        <div class="row justify-content-start">
          <div class="col-md-7">
            <img src="img/login/Group 586@2x.png" alt="Timedoor Coding Academy" width="452" class="img-fluid">
          </div>
          <div class="col-md-4">
            <h1 class="banner__title">Welcome Back</h1>
            <p class="banner__desc">Log in to Timedoor Coding Academy for Adult</p>
            <form class="form" method="POST" action="dashboard.php">
              <div class="form-group mb-4">
                <label for="emailusername" class="form__title">
                  <i class="far fa-envelope mr-2"></i> Email/Username</label>
                <input type="email" class="form-control form__input" id="emailusername">
              </div>
              <div class="form-group mb-4">
                <div class="d-flex justify-content-between">
                  <label for="password" class="form__title">
                    <img src="img/icon/icon-lock.svg" alt="Password" class="mr-2"> Password
                  </label>
                  <a class="form__title--green" data-toggle="modal" data-target="#exampleModal">Forgot Password?</a>
                </div>
                <div class="input-password">
                  <input type="password" class="form-control form__input--password" id="password">
                  <i class="far fa-eye input-password__icon"></i>
                </div>
              </div>
              <div class="form-group form-check">
                <input type="checkbox" class="form-check-input form__checkbox" id="rememberMe">
                <label class="form-check-label form__remember" for="rememberMe">Remember me</label>
              </div>
              <button type="submit" class="btn btn-dark btn-block">Login</button>
              <p class="form__member text-center">Not a member yet ? <a href="sign-up.php"
                  class="form__member--green">Sign Up Now</a></p>
            </form>
          </div>
        </div>
      </div>
    </section>
  </main>

  <!-- Modal -->
  <div class="modal fade modal-custom" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content modal-custom">
        <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-times custom-close__button"></i></span>
        </button>
        <div class="modal-body text-center">
          <img src="img/login/forgot.svg" alt="Forgot Password">
          <h3 class="modal-custom__title--small">Forgot Your Password?</h3>
          <p class="modal-custom__desc">
            Enter your email and we'll send you instructions to reset your password
          </p>
          <div class="form-group text-left">
            <label for="emailusernameModal" class="form__title">
              <i class="far fa-envelope mr-2"></i> Email</label>
            <input type="email" class="form-control form__input" id="emailusernameModal">
          </div>
          <button type="submit" class="btn btn-dark w-100">Send Reset Link</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Form -->
  <script src="js/form.js"></script>

</body>

</html>