<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/learning-video.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Learning Video - Timedoor Coding Academy</title>
</head>

<body>
  <?php require_once "component/header.php" ?>
  <section class="header-chapter">
    <div class="container container-custom">
      <div class="header-chapter__container">
        <div class="header-chapter__left">
          <a href="" class="btn-back"><i class="fas fa-angle-left fa-lg"></i></a>
          <h1 class="header-chapter__title">Our Course Subscription</h1>
        </div>
        <div class="header-chapter__right">
          <i class="fas fa-bars fa-2x side-bar__btn-show"></i>
        </div>
      </div>
    </div>
  </section>


  <!-- Side Bar -->
  <?php require_once "component/side-bar.php" ?>
  
  <main>
    <section class="video d-flex justify-content-center">
      <img src="img/video-example-4.png" alt="Video" class="video__img" width="100%">
      <span class="video__button" data-toggle="modal" data-target="#modalFinished"><i class="fas fa-play"></i></span>
    </section>
  </main>

  <!-- Modal -->
  <div class="modal fade modal-custom" id="modalFinished" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content modal-custom">
        <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-times custom-close__button"></i></span>
        </button>
        <div class="modal-body text-center">
          <img src="img/modal-check-green.svg" alt="Finished">
          <h3 class="modal-custom__title--small">You have finished trial course</h3>
          <p class="modal-custom__desc">
            You already finished learning this material, let's continue to the next material
          </p>
          <a href="course-subscription.php" type="submit" class="btn btn-dark modal-custom__btn">Continue</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- This Sidebar JS -->
  <script src="js/side-bar.js"></script>

  <!-- This Page JS -->
  <!-- <script src="js/pages/learning-video.js"></script> -->
</body>

</html>