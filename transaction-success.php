<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/sign-up.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Transaction Success - Timedoor Coding Academy</title>
</head>

<body class="sign-up-bg-success">
  <header>
    <div class="header py-3">
      <div class="container">
        <nav class="navbar navbar-dark bg-transparent">
          <a class="navbar-brand" href="#">
            <img src="img/timedoor-logo-black.png" width="168" height="39" alt="Timedoor Logo">
          </a>
        </nav>
      </div>
    </div>
  </header>
  <main>
    <section class="success pt-5">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 text-center">
            <img src="img/sign-up/success-icon.svg" alt="sucess" width="234" height="98" class="success__icon">
          </div>
          <div class="col-12">
            <h1 class="success__title">Transaction complete!</h1>
            <p class="success__desc">Now you can try our trial class and experiencing learning coding.</p>
          </div>
          <div class="col-8 text-center">
            <a href="dashboard.php" class="btn btn-dark--wide px-5">Go to Dashboard</a>
          </div>
        </div>
      </div>
    </section>
  </main>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-12 mt-5">
          <p class="form__member text-center">By signing up for Techdemia, you agree to Techdemia's
          <a class="form__member--green d-block">Terms of Service & Privacy Policy.</a></p>
        </div>
      </div>
    </div>
  </footer>
</body>

</html>