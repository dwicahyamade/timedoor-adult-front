<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@300&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/coding-practice.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Coding Practice - Timedoor Coding Academy</title>
</head>

<body>
  <?php require_once "component/header.php" ?>
  <section class="header-chapter">
    <div class="container container-custom">
      <div class="header-chapter__container">
        <div class="header-chapter__left">
          <a href="" class="btn-back"><i class="fas fa-angle-left fa-lg"></i></a>
          <h1 class="header-chapter__title">Coding Practice</h1>
          <p class="header-chapter__subtitle">Free Trial Introduction</p>
        </div>
        <div class="header-chapter__right">
          <div class="progress-bar-custom">
            <div class="progress-bar-custom__item active progress-bar-custom__line">1</div>
            <div class="progress-bar-custom__item active progress-bar-custom__line">2</div>
            <div class="progress-bar-custom__item progress-bar-custom__line">3</div>
            <div class="progress-bar-custom__item progress-bar-custom__line">4</div>
            <div class="progress-bar-custom__item progress-bar-custom__line">5</div>
          </div>
          <i class="fas fa-bars fa-2x side-bar__btn-show ml-5"></i>
        </div>
      </div>
    </div>
  </section>


  <!-- Side Bar -->
  <?php require_once "component/side-bar.php" ?>

  <main>
    <section class="coding">
      <div class="container-fluid">
        <div class="row">
          <div class="col-4 px-0">
            <div class="coding__left" id="coding-left">
              <h2 class="coding__left-title">Bold, Italic, Underline</h2>
              <div class="coding__left-container">
                <div class="coding__left-head">
                  <span class="coding__left-number">2</span>
                  <h3 class="coding__left-subtitle">
                    Change the word "Ursus Arctos" in the paragraph description of The
                    Brown Bear to Italic.</h3>
                </div>
                <p class="coding__left-desc">
                  In creating text content on the website, you can use a font style. There are several font styles that
                  can be used in content on website using HTML, namely bold, italic and underline. You can use each of
                  these styles to emphasize text in website content. Below is a more detailed video about font styles on
                  websites using HTML.
                </p>

                <div class="coding__left-video">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/mU6anWqZJcc"
                      title="YouTube video player" frameborder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                      allowfullscreen></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-8 px-0">
            <div class="coding__right editor">
              <?php require_once "component/sidebar-editor.php" ?>

              <!-- Editor Content -->
              <div class="editor__content">
                <div class="editor__head">
                  <ul class="editor__head-items">
                    <li class="editor__head-item active">HTML</li>
                    <li class="editor__head-item">CSS</li>
                    <li class="editor__head-item">Javascript</li>
                  </ul>
                </div>
                <div class="editor__text-wrapper">
                  <div id="editor">function foo(items) {
                    var x = "All this is syntax highlighted";
                    return x;
                    }
                  </div>
                  <div class="overlay">
                    <div class="overlay__icon-check">
                      <img src="img/icon/icon-checking-code.svg" alt="Checking Code">
                      <span class="d-block mt-2">Checking Code...</span>
                    </div>
                    <div class="overlay__icon overlay__error">
                      <img src="img/icon/icon-code-error.svg" alt="Code Error">
                      <span class="d-block mt-2 overlay__icon-title">Code Error</span>
                      <span class="d-block mt-2 overlay__icon-title overlay__icon-desc">There's some errors in your
                        code, check again</span>
                    </div>
                    <div class="overlay__icon overlay__success">
                      <img src="img/icon/icon-code-success.svg" alt="Code Error">
                      <span class="d-block mt-2 overlay__icon-title">All Code Correct</span>
                      <span class="d-block mt-2 overlay__icon-title overlay__icon-desc">Good job! Continue to next
                        practice</span>
                    </div>
                  </div>
                </div>
                <div class="editor__footer">
                  <span class="editor__footer-text">
                    Preview
                  </span>
                  <span class="test-error">test error</span>
                  <span class="test-correct">test correct</span>
                  <span class="test-finish" data-toggle="modal" data-target="#finishModal">test finish</span>
                  <button class="btn btn-green editor__footer-check">Check Code</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <!-- Modal -->
  <div class="modal fade modal-custom" id="finishModal" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content modal-custom">
        <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-times custom-close__button"></i></span>
        </button>
        <div class="modal-body text-center">
          <img src="img/modal-check-green.svg" alt="Finished">
          <h3 class="modal-custom__title--small">You have finished learning</h3>
          <p class="modal-custom__desc">
            You already finished learning this material, let's continue to the next material
          </p>
          <a href="learning-video-2.php" class="btn btn-dark modal-custom__btn">Continue Next Material</a>
          <a href="dashboard.php" class="modal-custom__footer-text">Back to Material List</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Robot Overlay -->
  <div class="robot-overlay">
    <div class="tab-content" id="nav-tabContent">

      <!-- Opening Tab -->
      <div class="tab-pane fade show active" id="robot-opening" role="tabpanel" aria-labelledby="robot-opening-tab">
        <div class="robot-overlay__image opening">
          <img src="img/robot/robot-wave-3.svg" alt="Robot">
          <div class="robot-overlay__text opening">
            <h3 class="robot-overlay__title">Hi! Ketemu lagi dengan Robot</h3>
            <p class="robot-overlay__desc mb-5">Penjelasan tentang coding practice. <br> Ayo kita mulai!</p>
            <div class="robot-overlay__info">
              <a class="nav-link robot-overlay__next ml-auto" id="robot-first-tab" data-toggle="tab" href="#robot-first" role="tab" aria-controls="robot-first" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- First Tab -->
      <div class="tab-pane fade" id="robot-first" role="tabpanel" aria-labelledby="robot-first-tab">
        <div class="robot-overlay__clone first"></div>
        <div class="robot-overlay__image first">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small first">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Material and Task</h3>
            <p class="robot-overlay__desc mb-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
            <div class="robot-overlay__info justify-content-between">
              <span class="robot-overlay__number">1/12</span>
              <a class="nav-link robot-overlay__next--small" id="robot-second-tab" data-toggle="tab" href="#robot-second" role="tab" aria-controls="robot-second" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Second Tab -->
      <div class="tab-pane fade" id="robot-second" role="tabpanel" aria-labelledby="robot-second-tab">
        <div class="robot-overlay__image second">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small second">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Task Number</h3>
            <p class="robot-overlay__desc mb-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
            <div class="robot-overlay__info">
              <span class="robot-overlay__number">2/12</span>
              <a class="nav-link robot-overlay__prev--small" id="robot-first-tab" data-toggle="tab" href="#robot-first" role="tab" aria-controls="robot-first" aria-selected="false">Prev</a>
              <a class="nav-link robot-overlay__next--small" id="robot-third-tab" data-toggle="tab" href="#robot-third" role="tab" aria-controls="robot-third" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Third Tab -->
      <div class="tab-pane fade" id="robot-third" role="tabpanel" aria-labelledby="robot-third-tab">
        <div class="robot-overlay__image third">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small third">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Menu</h3>
            <p class="robot-overlay__desc mb-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
            <div class="robot-overlay__info">
              <span class="robot-overlay__number">3/12</span>
              <a class="nav-link robot-overlay__prev--small" id="robot-second-tab" data-toggle="tab" href="#robot-second" role="tab" aria-controls="robot-second" aria-selected="false">Prev</a>
              <a class="nav-link robot-overlay__next--small" id="robot-fourth-tab" data-toggle="tab" href="#robot-fourth" role="tab" aria-controls="robot-fourth" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Fourth Tab -->
      <div class="tab-pane fade" id="robot-fourth" role="tabpanel" aria-labelledby="robot-fourth-tab">
        <div class="robot-overlay__clone fourth"></div>
        <div class="robot-overlay__image fourth">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small fourth">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Directory Tab</h3>
            <p class="robot-overlay__desc mb-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
            <div class="robot-overlay__info">
              <span class="robot-overlay__number">4/12</span>
              <a class="nav-link robot-overlay__prev--small" id="robot-third-tab" data-toggle="tab" href="#robot-third" role="tab" aria-controls="robot-third" aria-selected="false">Prev</a>
              <a class="nav-link robot-overlay__next--small" id="robot-fifth-tab" data-toggle="tab" href="#robot-fifth" role="tab" aria-controls="robot-fifth" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Fifth Tab -->
      <div class="tab-pane fade" id="robot-fifth" role="tabpanel" aria-labelledby="robot-fifth-tab">
        <div class="robot-overlay__clone fifth"></div>
        <div class="robot-overlay__image fifth">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small fifth">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Upload New Item</h3>
            <p class="robot-overlay__desc mb-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
            <div class="robot-overlay__info">
              <span class="robot-overlay__number">5/12</span>
              <a class="nav-link robot-overlay__prev--small" id="robot-fourth-tab" data-toggle="tab" href="#robot-fourth" role="tab" aria-controls="robot-fourth" aria-selected="false">Prev</a>
              <a class="nav-link robot-overlay__next--small" id="robot-sixth-tab" data-toggle="tab" href="#robot-sixth" role="tab" aria-controls="robot-sixth" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Sixth Tab -->
      <div class="tab-pane fade" id="robot-sixth" role="tabpanel" aria-labelledby="robot-sixth-tab">
        <div class="robot-overlay__clone sixth"></div>
        <div class="robot-overlay__image sixth">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small sixth">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Code Editor</h3>
            <p class="robot-overlay__desc mb-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
            <div class="robot-overlay__info">
              <span class="robot-overlay__number">6/12</span>
              <a class="nav-link robot-overlay__prev--small" id="robot-fifth-tab" data-toggle="tab" href="#robot-fifth" role="tab" aria-controls="robot-fifth" aria-selected="false">Prev</a>
              <a class="nav-link robot-overlay__next--small" id="robot-eighth-tab" data-toggle="tab" href="#robot-eighth" role="tab" aria-controls="robot-eighth" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Eighth Tab -->
      <div class="tab-pane fade" id="robot-eighth" role="tabpanel" aria-labelledby="robot-eighth-tab">
        <div class="robot-overlay__clone eighth"></div>
        <div class="robot-overlay__image eighth">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small eighth">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Style Tab (HTML, CSS, JS)</h3>
            <p class="robot-overlay__desc mb-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
            <div class="robot-overlay__info">
              <span class="robot-overlay__number">8/12</span>
              <a class="nav-link robot-overlay__prev--small" id="robot-sixth-tab" data-toggle="tab" href="#robot-sixth" role="tab" aria-controls="robot-sixth" aria-selected="false">Prev</a>
              <a class="nav-link robot-overlay__next--small" id="robot-ninth-tab" data-toggle="tab" href="#robot-ninth" role="tab" aria-controls="robot-ninth" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Ninth Tab -->
      <div class="tab-pane fade" id="robot-ninth" role="tabpanel" aria-labelledby="robot-ninth-tab">
        <div class="robot-overlay__clone ninth"></div>
        <div class="robot-overlay__image ninth">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small ninth">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Code Checker</h3>
            <p class="robot-overlay__desc mb-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
            <div class="robot-overlay__info">
              <span class="robot-overlay__number">9/12</span>
              <a class="nav-link robot-overlay__prev--small" id="robot-eighth-tab" data-toggle="tab" href="#robot-eighth" role="tab" aria-controls="robot-eighth" aria-selected="false">Prev</a>
              <a class="nav-link robot-overlay__next--small" id="robot-tenth-tab" data-toggle="tab" href="#robot-tenth" role="tab" aria-controls="robot-tenth" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Tenth Tab -->
      <div class="tab-pane fade" id="robot-tenth" role="tabpanel" aria-labelledby="robot-tenth-tab">
        <div class="robot-overlay__clone tenth"></div>
        <div class="robot-overlay__image ninth">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small ninth">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Code Checker</h3>
            <p class="robot-overlay__desc text-center">INI GAMBAR</p>
            <p class="robot-overlay__desc mb-4">Penjelasan Error</p>
            <div class="robot-overlay__info">
              <span class="robot-overlay__number">10/12</span>
              <a class="nav-link robot-overlay__prev--small" id="robot-ninth-tab" data-toggle="tab" href="#robot-ninth" role="tab" aria-controls="robot-ninth" aria-selected="false">Prev</a>
              <a class="nav-link robot-overlay__next--small" id="robot-eleventh-tab" data-toggle="tab" href="#robot-eleventh" role="tab" aria-controls="robot-eleventh" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Eleventh Tab -->
      <div class="tab-pane fade" id="robot-eleventh" role="tabpanel" aria-labelledby="robot-eleventh-tab">
        <div class="robot-overlay__clone eleventh"></div>
        <div class="robot-overlay__image ninth">
          <img src="img/robot/robot-idle-2-no-bg.svg" alt="Robot">
          <div class="robot-overlay__text--small ninth">
            <div class="robot-overlay__close"><i class="fas fa-times"></i></div>
            <h3 class="robot-overlay__title--small">Code Checker</h3>
            <p class="robot-overlay__desc text-center">INI GAMBAR</p>
            <p class="robot-overlay__desc mb-4">Penjelasan Berhasil</p>
            <div class="robot-overlay__info">
              <span class="robot-overlay__number">11/12</span>
              <a class="nav-link robot-overlay__prev--small" id="robot-tenth-tab" data-toggle="tab" href="#robot-tenth" role="tab" aria-controls="robot-tenth" aria-selected="false">Prev</a>
              <a class="nav-link robot-overlay__next--small" id="robot-finish-tab" data-toggle="tab" href="#robot-finish" role="tab" aria-controls="robot-finish" aria-selected="false">Next</a>
            </div>
          </div>
        </div>
      </div>

      <!-- Finish Tab -->
      <div class="tab-pane fade" id="robot-finish" role="tabpanel" aria-labelledby="robot-finish-tab">
        <div class="robot-overlay__image opening">
          <img src="img/robot/robot-wave-3.svg" alt="Robot">
          <div class="robot-overlay__text opening">
            <p class="robot-overlay__desc mb-5 text-center px-5">Itu dia pengenalan coding practice, sekarang sudah siap untuk mencoba?</p>
            <div class="d-blok text-center">
              <a href="#" class="btn btn-green robot-overlay__finish">Let's Start</a>
            </div>
          </div>
        </div>
      </div>

    </div>




    <!-- <div class="robot-overlay__image">
      <img src="img/robot/robot-wave-3.svg" alt="Robot">
    </div>
    <div class="robot-overlay__text-container">
      <div class="robot-overlay__text">
        <h3 class="robot-overlay__title">Hi! Ketemu lagi dengan Robot</h3>
        <p class="robot-overlay__desc mb-5">Penjelasan tentang coding practice <br>
        Ayo kita mulai!</p>
        <a href="#" class="robot-overlay__next">Next</a>
      </div>
    </div> -->
  </div>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- This Sidebar JS -->
  <script src="js/side-bar.js"></script>

  <!-- Ace Editor -->
  <script src="js/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>

  <!-- This Page JS -->
  <script src="js/pages/coding-practice.js"></script>

  <script>
    const navLink = document.querySelector('.nav')
  </script>
</body>

</html>