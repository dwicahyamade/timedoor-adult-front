<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/dashboard.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Dashboard - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "component/header.php" ?>
  <main>
    <div class="container">
      <div class="jumbotron jumbotron-custom">
        <div class="jumbotron-custom__text w-50">
          <h1 class="jumbotron-custom__title">Welcome, Setyo Syahindra</h1>
          <p class="jumbotron-custom__desc">Let's bost your skill with flexible learning. You've been learning
            building a website using HTML & CSS, Keep going on your progress!</p>
        </div>
        <div class="jumbotron-custom__image">
          <img src="img/dashboard-jumbotron.png" alt="Timedoor" class="img-fluid">
        </div>
      </div>

      <section class="recent section">
        <h2 class="section__title">Recent Learning</h2>
        <div class="row">
          <div class="col-md-8">
            <div class="recent__left">
              <p class="section__title--small">On Going Course</p>
              <h3 class="section__title m-0">Build a Website With HTML & CSS</h3>
              <p class="section__title--muted mb-4">Your progress path</p>
              <div class="d-flex justify-content-between">
                <div class="progress progress-custom">
                  <div class="progress-bar progress-custom__bar" role="progressbar" style="width: 50%"
                    aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                  <span class="progress-custom__percent">50%</span>
                </div>
                <a href="#" class="btn btn-dark--small">Resume</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 pl-0 d-flex">
            <div class="recent__right">
              <h3 class="section__title--top mb-1">
                Subscription Status
              </h3>
              <span class="recent__indicator">Active</span>
              <div class="row mt-3">
                <div class="col-lg-6 border-right">
                  <p class="d-block recent__joined">Joined Date</p>
                  <p class="d-block recent__date">10-10-2021</p>
                  <small class="recent__time">10.23 PM</small>
                </div>
                <div class="col-lg-6">
                  <p class="d-block recent__joined">Last Learning</p>
                  <p class="d-block recent__date">10-10-2021</p>
                  <small class="recent__time">07.23 PM</small>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="freetrial section">
        <h2 class="section__title--top">My Course</h2>
        <div class="d-flex justify-content-between">
          <h3 class="section__title">Free Trial Course</h3>
          <a href="chapter-opening-trial.php" class="btn btn-dark--wide">Start Free Trial</a>
        </div>
        
        <div class="section__bg">
          <!-- Introduction -->
          <div class="accordion" id="Introduction">
            <div class="chapter">
              <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                <button class="btn btn-accordion btn-block" type="button" data-toggle="collapse"
                  data-target="#introduction" aria-expanded="true" aria-controls="introduction">
                  <h3 class="section__title my-2">Introduction</h3>
                  <span class="btn-accordion__icon"></span>
                </button>
              </div>
              <div id="introduction" class="collapse show" aria-labelledby="headingOne" data-parent="#Introduction">
                <div class="accordion" id="IntroductionTopic">
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#topicOne" aria-expanded="true" aria-controls="topicOne"
                      id="topic">
                      <h4 class="section__title--top topic__title">Topic 1 - Digital Introduction</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-100 style-2">
                          <span class="label">100<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will learn the importance of digital technology and why we
                        need
                        to learn programming
                      </p>
                    </button>
                    <div id="topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#IntroductionTopic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                        <p class="topic__progress-text">Hello World</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                        <p class="topic__progress-text">Italic, bold, underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                        <p class="topic__progress-text">Image</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Outline of material</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Explain mentor system</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#topicTwo" aria-expanded="true" aria-controls="topicTwo">
                      <h4 class="section__title--top topic__title">Topic 2 - Coding Practice</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-25 style-2">
                          <span class="label">25<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will learn the importance of digital technology and why we
                        need
                        to learn programming
                      </p>
                    </button>
                    <div id="topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#IntroductionTopic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                        <p class="topic__progress-text">Hello World</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                        <p class="topic__progress-text">Italic, bold, underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                        <p class="topic__progress-text">Image</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Outline of material</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Explain mentor system</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#topicThree" aria-expanded="true"
                      aria-controls="topicThree">
                      <h4 class="section__title--top topic__title">Topic 3 - Introduction of HTML</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will learn the importance of digital technology and why we
                        need
                        to learn programming
                      </p>
                    </button>
                    <div id="topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#IntroductionTopic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                        <p class="topic__progress-text">Hello World</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                        <p class="topic__progress-text">Italic, bold, underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="fas fa-pen mr-2"></i> Practice</a>
                        <p class="topic__progress-text">Image</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Outline of material</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small mx-3"><i class="far fa-clone mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Explain mentor system</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>

      <section class="subscribe-course section">
        <div class="d-flex justify-content-between">
          <h2 class="section__title--top">Web Development Course</h2>
          <a href="course-subscription.php" class="btn btn-dark--wide">Subscribe Course</a>
        </div>
        <div class="d-flex justify-content-between">
          <h3 class="section__title">Build a Website With HTML & CSS</h3>
          <p class="subscribe-course__note">* Subscribe for full courses</p>
        </div>

        <div class="section__bg">
          <div class="accordion" id="Subscribe-Course">
            <!-- Chapter 1 -->
            <div class="chapter">
              <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                <button class="btn btn-accordion btn-block" type="button" data-toggle="collapse"
                  data-target="#chapterOne" aria-expanded="true" aria-controls="chapterOne">
                  <h3 class="section__title my-2">Chapter 1 - Basic Concept</h3>
                  <span class="btn-accordion__icon"></span>
                </button>
              </div>
              <div id="chapterOne" class="collapse show" aria-labelledby="headingOne"
                data-parent="#Subscribe-Course">
                <div class="accordion" id="chapterOne-topic">
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterOne-topicOne" aria-expanded="true"
                      aria-controls="chapterOne-topicOne">
                      <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterOne-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterOne-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="coding-submission.php" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submission</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterOne-topicTwo" aria-expanded="true"
                      aria-controls="chapterOne-topicTwo">
                      <h4 class="section__title--top topic__title">Topic 2 - Coding Practice</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterOne-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterOne-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterOne-topicThree" aria-expanded="true"
                      aria-controls="chapterOne-topicThree">
                      <h4 class="section__title--top topic__title">Topic 3 - IT Business Literacy</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterOne-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterOne-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <button class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</button>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterOne-topicFour" aria-expanded="true"
                      aria-controls="chapterOne-topicFour">
                      <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterOne-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterOne-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i class="fas fa-graduation-cap mr-2"></i>
                          Exam</a>
                        <p class="topic__progress-text">Final Exam</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <!-- Chapter 2 -->
            <div class="chapter">
              <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                <button class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
                  data-target="#chapterTwo" aria-expanded="true" aria-controls="chapterTwo">
                  <h3 class="section__title my-2">Chapter 2 - Basic HTML and CSS</h3>
                  <span class="btn-accordion__icon"></span>
                </button>
              </div>
              <div id="chapterTwo" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
                <div class="accordion" id="chapterTwo-topic">
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterTwo-topicOne" aria-expanded="true"
                      aria-controls="chapterTwo-topicOne">
                      <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterTwo-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterTwo-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterTwo-topicTwo" aria-expanded="true"
                      aria-controls="chapterTwo-topicTwo">
                      <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterTwo-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterTwo-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterTwo-topicThree" aria-expanded="true"
                      aria-controls="chapterTwo-topicThree">
                      <h4 class="section__title--top topic__title">Topic 3 - Division</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterTwo-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterTwo-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterOne-topicFour" aria-expanded="true"
                      aria-controls="chapterOne-topicFour">
                      <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterOne-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterOne-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-graduation-cap mr-2"></i>
                          Exam</a>
                        <p class="topic__progress-text">Final Exam</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <!-- Chapter 3 -->
            <div class="chapter">
              <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                <button class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
                  data-target="#chapterThree" aria-expanded="true" aria-controls="chapterThree">
                  <h3 class="section__title my-2">Chapter 3 - HTML & CSS Level Up</h3>
                  <span class="btn-accordion__icon"></span>
                </button>
              </div>
              <div id="chapterThree" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
                <div class="accordion" id="chapterThree-topic">
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterThree-topicOne" aria-expanded="true"
                      aria-controls="chapterThree-topicOne">
                      <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterThree-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterThree-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterThree-topicTwo" aria-expanded="true"
                      aria-controls="chapterThree-topicTwo">
                      <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterThree-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterThree-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterThree-topicThree" aria-expanded="true"
                      aria-controls="chapterThree-topicThree">
                      <h4 class="section__title--top topic__title">Topic 3 - Division</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterThree-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterThree-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterOne-topicFour" aria-expanded="true"
                      aria-controls="chapterOne-topicFour">
                      <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterOne-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterOne-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-graduation-cap mr-2"></i>
                          Exam</a>
                        <p class="topic__progress-text">Final Exam</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <!-- Chapter 4 -->
            <div class="chapter">
              <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                <button class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
                  data-target="#chapterFour" aria-expanded="true" aria-controls="chapterFour">
                  <h3 class="section__title my-2">Chapter 4 - Javascript</h3>
                  <span class="btn-accordion__icon"></span>
                </button>
              </div>
              <div id="chapterFour" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
                <div class="accordion" id="chapterFour-topic">
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterFour-topicOne" aria-expanded="true"
                      aria-controls="chapterFour-topicOne">
                      <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterFour-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterFour-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterFour-topicTwo" aria-expanded="true"
                      aria-controls="chapterFour-topicTwo">
                      <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterFour-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterFour-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterFour-topicThree" aria-expanded="true"
                      aria-controls="chapterFour-topicThree">
                      <h4 class="section__title--top topic__title">Topic 3 - Division</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterFour-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterFour-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterOne-topicFour" aria-expanded="true"
                      aria-controls="chapterOne-topicFour">
                      <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterOne-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterOne-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-graduation-cap mr-2"></i>
                          Exam</a>
                        <p class="topic__progress-text">Final Exam</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <!-- Chapter 5 -->
            <div class="chapter">
              <div class="card-header border-0 p-0 bg-transparent" id="headingOne">
                <button class="btn btn-accordion btn-block collapsed" type="button" data-toggle="collapse"
                  data-target="#chapterFive" aria-expanded="true" aria-controls="chapterFive">
                  <h3 class="section__title my-2">Chapter 5 - Vue JS</h3>
                  <span class="btn-accordion__icon"></span>
                </button>
              </div>
              <div id="chapterFive" class="collapse" aria-labelledby="headingOne" data-parent="#Subscribe-Course">
                <div class="accordion" id="chapterFive-topic">
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterFive-topicOne" aria-expanded="true"
                      aria-controls="chapterFive-topicOne">
                      <h4 class="section__title--top topic__title">Topic 1 - Show Content : Text</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterFive-topicOne" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterFive-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterFive-topicTwo" aria-expanded="true"
                      aria-controls="chapterFive-topicTwo">
                      <h4 class="section__title--top topic__title">Topic 2 - Show Other Content</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterFive-topicTwo" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterFive-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterFive-topicThree" aria-expanded="true"
                      aria-controls="chapterFive-topicThree">
                      <h4 class="section__title--top topic__title">Topic 3 - Division</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterFive-topicThree" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterFive-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Paragraph
                          <span class="topic__progress-text--small">15 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Heading
                          <span class="topic__progress-text--small">7 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Bold, Italic, Underline</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 1</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Color, Font Family
                          <span class="topic__progress-text--small">10 min 12 sec</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 2</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-play mr-2"></i>
                          Video</a>
                        <p class="topic__progress-text">Unordered List
                          <span class="topic__progress-text--small">12 min</span>
                        </p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="far fa-clone mr-2"></i>
                          Slide</a>
                        <p class="topic__progress-text">Ordered List</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i class="fas fa-pen mr-2"></i>
                          Practice</a>
                        <p class="topic__progress-text">Coding Practice 3</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-code mr-2"></i>
                          Project</a>
                        <p class="topic__progress-text">Coding Submision</p>
                      </div>
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button mx-3"><i
                            class="fas fa-puzzle-piece mr-2"></i>
                          Quiz</a>
                        <p class="topic__progress-text">Showing Text Content</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-border">
                    <button class="card-body btn-accordion-inner d-flex topic btn btn-block collapsed" type="button"
                      data-toggle="collapse" data-target="#chapterOne-topicFour" aria-expanded="true"
                      aria-controls="chapterOne-topicFour">
                      <h4 class="section__title--top topic__title">Topic 4 - Final Exam</h4>
                      <div class="topic__progress">
                        <div class="pie-wrapper progress-0 style-2">
                          <span class="label">0<span class="smaller">%</span></span>
                          <div class="pie">
                            <div class="left-side half-circle"></div>
                            <div class="right-side half-circle"></div>
                          </div>
                          <div class="pie-shadow"></div>
                        </div>
                      </div>
                      <p class="topic__desc">This topic will contain material about introduction to the internet.
                        Material includes what is the internet, history, and the opportunities of the internet.
                      </p>
                    </button>
                    <div id="chapterOne-topicFour" class="collapse topic-inner" aria-labelledby="headingOne"
                      data-parent="#chapterOne-topic">
                      <div class="topic-inner__detail my-2">
                        <div class="form-check">
                          <input class="form-check-input topic__checkbox" type="checkbox" id="blankCheckbox"
                            value="option1">
                        </div>
                        <a href="#" class="btn btn-dark--small topic-inner__button topic-inner__button mx-3"><i
                            class="fas fa-graduation-cap mr-2"></i>
                          Exam</a>
                        <p class="topic__progress-text">Final Exam</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>
    </div>

    <!-- whatsapp & back to top -->
    <?php require_once 'component/button-whatsapp.php' ?>

    <?php require_once 'component/button-back-to-top.php' ?>
    
  </main>

  <?php require_once "component/footer.php" ?>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Layout -->
  <script src="js/pages/layout.js"></script>

  <!-- This Page Only JS -->
  <script src="js/pages/dashboard.js"></script>
</body>

</html>