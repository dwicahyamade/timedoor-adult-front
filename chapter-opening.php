<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/chapter-opening.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Chapter 1 - Timedoor Coding Academy</title>
</head>

<body class="body">
  <?php require_once "component/header.php" ?>
  <main>
    <div class="container">
      <section class="chapter">
        <div class="chapter__bg">
          <h1 class="chapter__title">Chapter 2 : Basic HTML and CSS</h1>
          <h2 class="chapter__subtitle">Topic 1 - Show Content : Text</h2>
          <p class="chapter__desc">This topic will contain material about introduction to the internet. Material
            includes what is the internet, history, and the opportunities of the internet.</p>
          <a href="learning-video.php" class="btn btn-dark w-50">Start Learning</a>
        </div>
      </section>
    </div>
  </main>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
</body>

</html>