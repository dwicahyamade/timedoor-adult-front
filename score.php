<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
  <meta name="robots" content="noindex, nofollow" />

  <!-- Fonts Google -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

  <!-- UNIVERSAL CSS -->
  <link rel="stylesheet" href="css/layout.css">

  <!--  THIS PAGE ONLY CSS -->
  <link rel="stylesheet" href="css/pages/score.css">

  <!-- Faveicon -->
  <link rel="shortcut icon" type="image/png" href="img/faveicon/timedoor-faveicon.jpg">

  <title>Score - Timedoor Coding Academy</title>
</head>

<body>
  <?php require_once "component/header.php" ?>
  <section class="header-chapter">
    <div class="container container-custom">
      <div class="header-chapter__container">
        <div class="header-chapter__left">
          <a href="" class="btn-back"><i class="fas fa-angle-left fa-lg"></i></a>
          <h1 class="header-chapter__title">Chapter 2</h1>
          <p class="header-chapter__subtitle">Topic 1 - Color, Font Family</p>
        </div>
        <div class="header-chapter__right">
          <i class="fas fa-bars fa-2x side-bar__btn-show"></i>
        </div>
      </div>
    </div>
  </section>


  <!-- Side Bar -->
  <?php require_once "component/side-bar.php" ?>

  <main>
    <section class="score">
      <div class="score__bg">
        <h1 class="score__title">You did a great job!</h1>
        <p class="score__subtitle">Your Score is
          <span class="score__score">90</span>
        </p>
        <p class="score__desc">You are a fast learner! keep it up <br>
          Let's continue to learn more in next topic</p>
        <a href="" class="btn btn-dark">Continue to Next Topic</a>
        <a href="dashboard.php" class="score__back">Back to Material List</a>
      </div>
    </section>
  </main>

  <!-- Bootstrap -->
  <script src="js/jquery-3.5.1.slim.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- This Sidebar JS -->
  <script src="js/side-bar.js"></script>

  <!-- This Page JS -->
</body>

</html>